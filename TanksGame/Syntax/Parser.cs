﻿using TanksGame.Objects;
using TanksGame.Objects.ReplyObjects;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
//using System.Drawing;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TanksGame.Syntax
{
	/// <summary>
	/// Used to extract information from server reply string
	/// </summary>
	public class Parser
	{
		#region Declerations
		private static Parser instance;
		#endregion

		#region Constructor

		/// <summary>
		///  Returns the only instance of the class
		/// </summary>
		public static Parser GetInstance
		{
			get
			{
				if (instance == null)
				{
					Console.WriteLine("First instance");
					instance = new Parser();
				}
				return instance;
			}
			set
			{
				instance = value;
			}
		}

		/// <summary>
		/// Do not instantiate
		/// </summary>
		private Parser()
		{
		}

		#endregion

		#region Parser method

		/// <summary>
		/// Parse string sent by the game server to game objects
		/// </summary>
		/// <param name="serverString">String sent by the server</param>
		/// <returns>A reply object with msg type and data if available</returns>
		public Reply Parse(String serverString)
		{
            Console.WriteLine("Parse Invoked");
			if (serverString == null)
			{
                Console.WriteLine("null serverString");
				throw new ArgumentNullException("Nothing to parse");
			}
			if (String.IsNullOrEmpty(serverString.Trim()))
			{
                Console.WriteLine("empty serverString");
				throw new FormatException("Empty serverString");
			}
			if (serverString.EndsWith("#"))
			{
				serverString = serverString.Substring(0, serverString.Length - 1);//Remove #
			}
			else
			{
                Console.WriteLine("String does not end with #");
			}

			Reply reply = new Reply();
			switch (serverString)
			{
				//Reply with no data
				// TODO: Handle Moving Shooting and No data Replies
				case "PLAYERS_FULL":
                    Console.WriteLine("Reply type - PLAYERS_FULL");
					reply.MessageType = ReplyTypes.PLAYERS_FULL;
					break;
				case "ALREADY_ADDED":
                    Console.WriteLine("Reply type - ALREADY_ADDED");
					reply.MessageType = ReplyTypes.ALREADY_ADDED;
					break;
				case "GAME_ALREADY_STARTED":
                    Console.WriteLine("Reply type - GAME_ALREADY_STARTED");
					reply.MessageType = ReplyTypes.GAME_ALREADY_STARTED;
					break;
				case "CELL_OCCUPIED":
                    Console.WriteLine("Reply type - CELL_OCCUPIED");
					reply.MessageType = ReplyTypes.CELL_OCCUPIED;
					break;
				case "PITFALL":
                    Console.WriteLine("Reply type - PITFALL");
					reply.MessageType = ReplyTypes.PITFALL;
					break;
				case "DEAD":
                    Console.WriteLine("Reply type - DEAD");
					reply.MessageType = ReplyTypes.DEAD;
					break;
				case "TOO_QUICK":
                    Console.WriteLine("Reply type - TOO_QUICK");
					reply.MessageType = ReplyTypes.TOO_QUICK;
					break;
				case "INVALID_CELL":
                    Console.WriteLine("Reply type - INVALID_CELL");
					reply.MessageType = ReplyTypes.INVALID_CELL;
					break;
				case "GAME_HAS_FINISHED":
                    Console.WriteLine("Reply type - GAME_HAS_FINISHED");
					reply.MessageType = ReplyTypes.GAME_HAS_FINISHED;
					break;
				case "GAME_NOT_STARTED_YET":
                    Console.WriteLine("Reply type - GAME_NOT_STARTED_YET");
					reply.MessageType = ReplyTypes.GAME_NOT_STARTED_YET;
					break;
				case "NOT_A_VALID_CONTESTANT":
                    Console.WriteLine("Reply type - NOT_A_VALID_CONTESTANT");
					reply.MessageType = ReplyTypes.NOT_A_VALID_CONTESTANT;
					break;
				case "GAME_FINISHED":
                    Console.WriteLine("Reply type - GAME_FINISHED");
					reply.MessageType = ReplyTypes.GAME_FINISHED;
					break;
				//Reply with data
				default:
                    Console.WriteLine("No match for basic reply type");
					if (serverString.StartsWith("S:"))
					{//Join request accepted
                        Console.WriteLine("Reply type - GAME_START");
						reply.MessageType = ReplyTypes.GAME_START;
						reply.Data = ParsePlayerAcceptance(serverString.Substring(2));
					}
					else if (serverString.StartsWith("I:"))
					{//Initial player details
                        Console.WriteLine("Reply type - MAP_DATA");
						reply.MessageType = ReplyTypes.MAP_DATA;
						reply.Data = ParseMapObstacles(serverString.Substring(2));
					}
					else if (serverString.StartsWith("G:"))
					{//Game variable update
                        Console.WriteLine("Reply type - GLOBAL_DATA_UPDATE");
						reply.MessageType = ReplyTypes.GLOBAL_DATA_UPDATE;
						reply.Data = ParseGameVariableUpdate(serverString.Substring(2));
					}
					else if (serverString.StartsWith("C:"))
					{//Coin pile
                        Console.WriteLine("Reply type - COIN_DATA");
						reply.MessageType = ReplyTypes.COIN_DATA;
						reply.Data = ParseCoinPile(serverString.Substring(2));
					}
					else if (serverString.StartsWith("L:"))
					{//Life pack
                        Console.WriteLine("Reply type - LIFE_PACK_DATA");
						reply.MessageType = ReplyTypes.LIFE_PACK_DATA;
						reply.Data = ParseLifePack(serverString.Substring(2));
					}
					else if (serverString.StartsWith("OBSTACLE"))
					{
                        Console.WriteLine("Reply type - OBSTACLE");
						reply.MessageType = ReplyTypes.OBSTACLE;
						reply.Data = ParseObstacle(serverString.Split(';')[1]);
					}
					break;
			}
			return reply;
		}

		#endregion

		#region Helper Methods

		
		/// <summary>
		/// Parse initial Player data
		/// </summary>
		/// <param name="str">Incomming data string</param>
		/// <returns>A game start information object -GameStartInfo</returns>
		private GameStartInfo ParsePlayerAcceptance(string str)
		{
            Console.WriteLine("ParsePlayerAcceptance Invoked");
			//Format  P<num>; < player location  x>,< player location  y>;<Direction>
			//P0;0,0;0 : P1;0,9;0 : P2;9,0;0

			String[] s = str.Split(':');

			List<Player> players = new List<Player>();

			foreach (string playerInfoStr in s)
			{
				String[] playerStr = playerInfoStr.Split(';');
				if (playerStr.Length != 3) throw new FormatException("Wrong player string format");

				string playerName = playerStr[0];
				string[] playerLoc = playerStr[1].Split(',');
				string direction = playerStr[2];

				//Get initial player direction
				int playerDirn = 0;
				// OverflowEx; FormatException Ex; !NullEx 
				playerDirn = int.Parse(direction);

				if (playerDirn < 0 || playerDirn > 3 || !playerName.StartsWith("P") || playerLoc.Length != 2) throw new FormatException("Invalid player information format");

				//Get player id
				int playerId = 0;
				// OverflowEx; FormatException Ex; !NullEx
				playerId = int.Parse(playerName.Substring(1));

				//Get initial player location
				Point pt = new Point(0, 0);
				// OverflowEx; FormatException Ex; !NullEx
				pt.X = int.Parse(playerLoc[0]);
				pt.Y = int.Parse(playerLoc[1]);

				//Wrap data in Player object
				Player p = new Player();
				p.PlayerID = playerId;
				p.Direction = (Directions)playerDirn;
				p.Location = pt;

				players.Add(p);
			}

			GameStartInfo gsi = new GameStartInfo();
			gsi.Players = players;
			return gsi;
		}

		/// <summary>
		/// Parse map obstacles
		/// </summary>
		/// <param name="str">Incomming data string</param>
		/// <returns>A map obstacle datail object</returns>
		private MapObstacle ParseMapObstacles(string str)
		{
            Console.WriteLine("ParseMapObstacles Invoked");
			//Format  P1: < x>,<y>;< x>,<y>;< x>,<y>…..< x>,<y>  : < x>,<y>;< x>,<y>;< x>,<y>…..< x>,<y>  : < x>,<y>;< x>,<y>;< x>,<y>…..< x>,<y>

			String[] strSplit = str.Split(':');

			if (strSplit.Length != 4) throw new FormatException("Incorrect number of map detail objects");

			string playerName = strSplit[0];
			string[] brickWallCoordinates = strSplit[1].Split(';');
			string[] stoneWallCoordinates = strSplit[2].Split(';');
			string[] waterBlockCoordinates = strSplit[3].Split(';');

			//Get client id
			int playerId = 0;
			// OverflowEx; FormatException Ex; !NullEx
			playerId = int.Parse(playerName.Substring(1));

			//Get brick wall coordinates
			List<Point> brickWalls = new List<Point>();
			foreach (string cord in brickWallCoordinates)
			{
				string[] brickWallLoc = cord.Split(',');
				if (brickWallLoc.Length != 2) throw new FormatException("Incorrect brick wall coordinate format");

				Point pt = new Point(0, 0);
				// OverflowEx; FormatException Ex; !NullEx
				pt.X = int.Parse(brickWallLoc[0]);
				pt.Y = int.Parse(brickWallLoc[1]);

				brickWalls.Add(pt);

			}

			//Get stone wall coordinates
			List<Point> stoneWalls = new List<Point>();
			foreach (string cord in stoneWallCoordinates)
			{
				string[] stoneWallLoc = cord.Split(',');
				if (stoneWallLoc.Length != 2) throw new FormatException("Incorrect stone wall coordinate format");

				Point pt = new Point(0, 0);
				// OverflowEx; FormatException Ex; !NullEx
				pt.X = int.Parse(stoneWallLoc[0]);
				pt.Y = int.Parse(stoneWallLoc[1]);

				stoneWalls.Add(pt);
			}

			//Get water block coordinates
			List<Point> waterBlocks = new List<Point>();
			foreach (string cord in waterBlockCoordinates)
			{
				string[] waterBlockLoc = cord.Split(',');
				if (waterBlockLoc.Length != 2) throw new FormatException("Incorrect water block coordinate format");

				Point pt = new Point(0, 0);
				// OverflowEx; FormatException Ex; !NullEx
				pt.X = int.Parse(waterBlockLoc[0]);
				pt.Y = int.Parse(waterBlockLoc[1]);

				waterBlocks.Add(pt);
			}

			//Check for no of obstacles
			//Brick walls 7
			//Stone walls 8
			//Water blocks 10
			//if (brickWalls.Count != 7 || stoneWalls.Count != 8 || waterBlocks.Count != 10) throw new FormatException();

			//Wrap data in MapObstacle Object
			MapObstacle mo = new MapObstacle();
			mo.ClientPlayerID = playerId;
			mo.BrickWalls = brickWalls;
			mo.StoneWalls = stoneWalls;
			mo.WaterBlocks = waterBlocks;

			return mo;
		}


		/// <summary>
		/// Parse game variables
		/// </summary>
		/// <param name="str">Incomming data string</param>
		/// <returns>A game variable update object</returns>
		private GameVariableUpdate ParseGameVariableUpdate(string str)
		{
            Console.WriteLine("ParseGameVariableUpdate Invoked");
			//Format P1;< player location  x>,< player location  y>;<Direction>;< whether shot>;<health>;< coins>;< points>: …. P5;< player location  x>,< player location  y>;<Direction>;< whether shot>;<health>;< coins>;< points>: < x>,<y>,<damage-level>;< x>,<y>,<damage-level>;< x>,<y>,<damage-level>;< x>,<y>,<damage-level>…..< x>,<y>,<damage-level>
			//eg  P0; 0,7;1;1;110;4766;4876 : P1;6,5;2;0;100;9584;9584 : P2;1,7;1;0;90;5175;5175 : 7,2,4 ; 8,6,0 ; 4,2,4 ; 5,7,0 ; 1,3,4 ; 7,8,0 ; 2,4,4

			String[] strSplit = str.Split(':');

			List<Player> players = new List<Player>();
			List<BrickWallDamageReport> brickWallDamage = new List<BrickWallDamageReport>();

			foreach (string updateInfStr in strSplit)
			{
				if (updateInfStr.StartsWith("P"))
				{//Player detail update
					String[] playerInfo = updateInfStr.Split(';');
					if (playerInfo.Length != 7) throw new FormatException("Incorrect player detail update format");
					/*
					0 - name [int]
					1 - coordinates point
					2 - direction (Directions)[int]
					3 - whether short [int]
					4 - health [int]
					5 - coins [long]
					6 - points [long]
					*/

					string playerName = playerInfo[0];
					string[] playerLoc = playerInfo[1].Split(',');
					string direction = playerInfo[2];
					string whetherShot = playerInfo[3];
					string health = playerInfo[4];
					string coins = playerInfo[5];
					string points = playerInfo[6];

					//Get initial player direction
					int playerDirn = 0;
					// OverflowEx; FormatException Ex; !NullEx 
					playerDirn = int.Parse(direction);

					if (playerDirn < 0 || playerDirn > 3 || !playerName.StartsWith("P") || playerLoc.Length != 2) throw new FormatException();

					//Get player id
					int playerId = 0;
					// OverflowEx; FormatException Ex; !NullEx
					playerId = int.Parse(playerName.Substring(1));

					//Get initial player location
					Point pt = new Point(0, 0);
					// OverflowEx; FormatException Ex; !NullEx
					pt.X = int.Parse(playerLoc[0]);
					pt.Y = int.Parse(playerLoc[1]);

					//Get whether player hit
					int whetherHit = 0;
					// OverflowEx; FormatException Ex; !NullEx
					whetherHit = int.Parse(whetherShot);

					//Get player health
					int playerHealth = 0;
					// OverflowEx; FormatException Ex; !NullEx
					playerHealth = int.Parse(health);

					//Get player coins
					long playerCoins = 0;
					// OverflowEx; FormatException Ex; !NullEx
					playerCoins = long.Parse(coins);

					//Get player points
					long playerPoints = 0;
					// OverflowEx; FormatException Ex; !NullEx
					playerPoints = long.Parse(points);

					//Wrap data in Player object
					Player p = new Player();
					p.PlayerID = playerId;
					p.Location = pt;
					p.Direction = (Directions)playerDirn;
					p.Shot = whetherHit;
					p.Health = playerHealth;
					p.Coins = playerCoins;
					p.Points = playerPoints;

					//Add p to players list
					players.Add(p);

				}
				else//Brick wall damage report
				{
					String[] brickWallsInfoStr = updateInfStr.Split(';');
					//if (brickWallsInfoStr.Length != 7) throw new FormatException();
					foreach (string brickWallDamageInfoStr in brickWallsInfoStr)
					{
						String[] brickWallDamageInfo = brickWallDamageInfoStr.Split(',');
						if (brickWallDamageInfo.Length != 3) throw new FormatException("Incorrect brick wall damage update");

						//Get brick wall coordinates
						Point pt = new Point(0, 0);
						// OverflowEx; FormatException Ex; !NullEx
						pt.X = int.Parse(brickWallDamageInfo[0]);
						pt.Y = int.Parse(brickWallDamageInfo[1]);

						//Get brick wall damage
						int damage = 0;
						// OverflowEx; FormatException Ex; !NullEx
						damage = int.Parse(brickWallDamageInfo[2]);

						//Wrap data in BrickWallDamageReport object
						BrickWallDamageReport bwdr = new BrickWallDamageReport();
						bwdr.Location = pt;
						bwdr.Damage = damage;

						//Add bwdr to brickWallDamage list
						brickWallDamage.Add(bwdr);
					}
				}
			}


			GameVariableUpdate gvu = new GameVariableUpdate();
			gvu.Players = players;
			gvu.BrickWallDamage = brickWallDamage;

			return gvu;
		}


		/// <summary>
		/// Parse coin Pile
		/// </summary>
		/// <param name="str">Incomming data string</param>
		/// <returns>A coin pile information object</returns>
		private CoinPile ParseCoinPile(string str)
		{
            Console.WriteLine("ParseCoinPile Invoked");
			//Format <x>,<y>:<LT>:<Val>
			String[] strSplit = str.Split(':');

			if (strSplit.Length != 3) throw new FormatException("Incorrect coin pile information format");

			//Get coin pile location
			String[] coinPileLoc = strSplit[0].Split(',');
			if (coinPileLoc.Length != 2) throw new FormatException("Incorrect coin pile coordinate format");
			Point pt = new Point(0, 0);
			// OverflowEx; FormatException Ex; !NullEx
			pt.X = int.Parse(coinPileLoc[0]);
			pt.Y = int.Parse(coinPileLoc[1]);

			//Get coin pile life time
			long lifeTime = 0;
			// OverflowEx; FormatException Ex; !NullEx
			lifeTime = long.Parse(strSplit[1]);


			//Get coin pile value
			long value = 0;
			// OverflowEx; FormatException Ex; !NullEx
			value = long.Parse(strSplit[2]);

			//Wrap data in CoinPile Object
			CoinPile cp = new CoinPile();
			cp.Location = pt;
			cp.LifeTime = lifeTime;
			cp.Value = value;

			return cp;
		}

		/// <summary>
		/// Parse life pack
		/// </summary>
		/// <param name="str">Incomming data string</param>
		/// <returns>A life pack information object</returns>
		private LifePack ParseLifePack(string str)
		{
            Console.WriteLine("ParseLifePack Invoked");
			//Format <x>,<y>:<LT>

			String[] strSplit = str.Split(':');

			if (strSplit.Length != 2) throw new FormatException("Incorrect life pack information format");

			//Get Life Pack location
			String[] lifePackLoc = strSplit[0].Split(',');
			if (lifePackLoc.Length != 2) throw new FormatException("Incorrect life pack coordinate format");
			Point pt = new Point(0, 0);

			// OverflowEx; FormatException Ex; !NullEx
			pt.X = int.Parse(lifePackLoc[0]);
			pt.Y = int.Parse(lifePackLoc[1]);

			//Get Life Pack life time
			long lifeTime = 0;
			// OverflowEx; FormatException Ex; !NullEx
			lifeTime = long.Parse(strSplit[1]);


			//Wrap data in Life Pack Object
			LifePack lp = new LifePack();
			lp.Location = pt;
			lp.LifeTime = lifeTime;

			return lp;

		}

		/// <summary>
		/// Parse obstacle
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		private Data ParseObstacle(string v)
		{
			Obstacle o = new Obstacle();
			o.Lost_Points = int.Parse(v);
			return o;
		}
	}
	#endregion
}
