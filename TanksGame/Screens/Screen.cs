﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Linq;

namespace TanksGame.Screens
{
    public abstract class Screen : DrawableGameComponent
    {
        #region Atrributes
        // List of Game Components
        List<GameComponent> components = new List<GameComponent>();
        // Game
        protected ScreenManager game;
        // SpriteBatch
        protected SpriteBatch spriteBatch;
        // Keyboard Management
        protected KeyboardState keyboardState;
        protected KeyboardState oldKeyboardState;
        #endregion

        #region Properties
        public List<GameComponent> Components
        {
            get { return components; }
        }
        #endregion

        #region Methods___
        /// <summary>
        /// GameScreen
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        public Screen(ScreenManager game, SpriteBatch spriteBatch) : base(game)
        {
            this.game = game;
            this.spriteBatch = spriteBatch;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Update Game
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            keyboardState = Keyboard.GetState();
            // Get all clisked(Pressed) keys
            Keys[] keys = GetPressedKeys();
            // Get currently pressed(down) keys
            Keys[] keysPressed = keyboardState.GetPressedKeys();
            UpdateScreen(keys, keysPressed, gameTime);
            foreach (GameComponent component in components)
                if (component.Enabled == true)
                    component.Update(gameTime);
            oldKeyboardState = keyboardState;
        }

        public abstract void UpdateScreen(Keys[] keys, Keys[] keysPressed, GameTime gameTime);

        /// <summary>
        /// Draw Game
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            foreach (GameComponent component in components)
                if (component is DrawableGameComponent &&
                ((DrawableGameComponent)component).Visible)
                    ((DrawableGameComponent)component).Draw(gameTime);
        }

        /// <summary>
        /// Set visibility of the game component to be true
        /// </summary>
        public virtual void Show()
        {
            this.Visible = true;
            this.Enabled = true;
            foreach (GameComponent component in components)
            {
                component.Enabled = true;
                if (component is DrawableGameComponent)
                    ((DrawableGameComponent)component).Visible = true;
            }
        }

        /// <summary>
        /// Set visibility of the game component to be false
        /// </summary>
        public virtual void Hide()
        {
            this.Visible = false;
            this.Enabled = false;
            foreach (GameComponent component in components)
            {
                component.Enabled = false;
                if (component is DrawableGameComponent)
                    ((DrawableGameComponent)component).Visible = false;
            }
        }

        /// <summary>
        /// Check for key Press
        /// </summary>
        /// <param name="aKey">The key to be checked</param>
        /// <returns></returns>
        private Keys[] GetPressedKeys()
        {
            return oldKeyboardState.GetPressedKeys().Except<Keys>(keyboardState.GetPressedKeys()).ToArray();
        }

        /// <summary>
        /// Checks the key for key press
        /// </summary>
        /// <param name="aKey"></param>
        /// <returns></returns>
        protected bool CheckKey(Keys aKey)
        {
            return oldKeyboardState.IsKeyDown(aKey) && keyboardState.IsKeyUp(aKey);
        }
        #endregion
    }
}
