﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TanksGame.Controls;
using TanksGame;

namespace TanksGame.Screens
{
    class SettingsScreen : Screen
    {
        // Background
        Texture2D image;
        Rectangle imageRectangle;

        // Components
        Panel pnlConnection;
        TextBox txtIpAddress;
        Label lblIpAddress;

        List subMenu;

        Label Help;

        private SpriteFont listFont;
        public SettingsScreen(ScreenManager game, SpriteBatch spriteBatch, Texture2D image, SpriteFont spriteFont) : base(game, spriteBatch)
        {
            this.image = image;
            imageRectangle = new Rectangle(0, 0, Game.Window.ClientBounds.Width, Game.Window.ClientBounds.Height);
            pnlConnection = new Panel(game, spriteBatch);
            pnlConnection.Position = new Vector2(250, 250);

            lblIpAddress = new Label(game, spriteBatch);
            lblIpAddress.Position = new Vector2(0, 0);
            lblIpAddress.Text = "IP Address:";
            pnlConnection.Add(lblIpAddress);

            txtIpAddress = new TextBox(game, spriteBatch);
            txtIpAddress.Position = lblIpAddress.Position + new Vector2(lblIpAddress.Size.X, 0);
            txtIpAddress.Text = GameSettings.ServerIpAddress;
            txtIpAddress.TextChanged += TxtIpAddress_TextChanged;
            txtIpAddress.Focus();
            pnlConnection.Add(txtIpAddress);

            Components.Add(pnlConnection);

            listFont = game.Content.Load<SpriteFont>("fonts/listFont");


            subMenu = new List(game, spriteBatch, new string[] { "Connection", "Back" });

            // Components
            subMenu.HiliteColor = Color.Red;
            subMenu.ForeColor = Color.Orange;
            subMenu.Font = listFont;

            Components.Add(subMenu);

            Help = new Label(game, spriteBatch);
            Help.Position = new Vector2(450, GameSettings.ReslutionHeight - 50);
            Help.Text = "Press <Enter> or <Right> to Select...";
            Components.Add(Help);
            subMenu.Focus();
        }

        private void TxtIpAddress_TextChanged(object sender, EventArgs e)
        {
            GameSettings.ServerIpAddress = txtIpAddress.Text;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Draw(image, imageRectangle, Color.White);
            base.Draw(gameTime);
        }

        public override void UpdateScreen(Keys[] keysClicked, Keys[] keysPressed, GameTime gameTime)
        {
            if (pnlConnection.IsFocused)
            {
                if (CheckKey(Keys.Left))
                {
                    pnlConnection.IsFocused = false;
                    subMenu.Focus();
                    Help.Text = "Press <Enter> or <Right> to Select...";
                }
            }
            else if (subMenu.IsFocused)
            {
                if (CheckKey(Keys.Right) || CheckKey(Keys.Enter))
                {
                    if (subMenu.SelectedIndex == 0)
                    {
                        subMenu.IsFocused = false;
                        pnlConnection.Focus();
                        Help.Text = "Press <Left> to go back to menu...";
                    }
                    if (subMenu.SelectedIndex == 1)
                    {
                        game.ActivateMainScreen();
                    }
                }
            }
        }
    }
}