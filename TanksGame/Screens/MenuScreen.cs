﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TanksGame.Controls;

namespace TanksGame.Screens
{
    class MenuScreen : Screen
    {
        #region Attributes
        private List menuComponent;
        private Texture2D image;
        private Rectangle imageRectangle;


        private SpriteFont listFont;
        #endregion

        #region Properties
        /// <summary>
        /// SelectIndex
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return menuComponent.SelectedIndex;
            }
            set
            {
                menuComponent.SelectedIndex = value;
            }
        }
        #endregion

        #region Methods___
        /// <summary>
        /// Game Menu Screen Initialize
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="spriteFont"></param>
        /// <param name="image"></param>
        public MenuScreen(ScreenManager game, SpriteBatch spriteBatch, SpriteFont spriteFont, Texture2D image) : base(game, spriteBatch)
        {
            imageRectangle = new Rectangle(0, 0, Game.Window.ClientBounds.Width, Game.Window.ClientBounds.Height);
            // Create Background image
            this.image = image;

            listFont = game.Content.Load<SpriteFont>("fonts/listFont");

            string[] menuItems = { "New Game", "Continue", "Settings", "Web Site", "Quit" };

            // Components
            menuComponent = new List(game, spriteBatch, menuItems);
            menuComponent.HiliteColor = Color.Red;
            menuComponent.ForeColor = Color.Orange;
            menuComponent.Font = listFont;

            Components.Add(menuComponent);
            menuComponent.Focus();
        }

        /// <summary>
        /// Update Screen
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// Draw screen
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Draw(image, imageRectangle, Color.White);
            base.Draw(gameTime);
        }

        public override void UpdateScreen(Keys[] keys, Keys[] keysPressed, GameTime gameTime)
        {
            // TODO:
        }
        #endregion
    }
}

