﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using TanksGame.Objects.ReplyObjects;
using TanksGame.Engine;
using TanksGame.AI;

namespace TanksGame.Screens
{
	class GameScreen : Screen
	{
		#region Declerations


		//Game engine refernece
		private GameEngine gameEngine;
		private GameAi gameAi;

		private PrimitiveBatch primitiveBatch;

		//Textures
		private Texture2D image;
		private Rectangle imageRectangle;

		private Texture2D backgroundTexture;

		private Texture2D playerTankNorth;
		private Texture2D playerTankEast;
		private Texture2D playerTankSouth;
		private Texture2D playerTankWest;

		private Texture2D empty;

		private Texture2D brickWall_0;
		private Texture2D brickWall_1;
		private Texture2D brickWall_2;
		private Texture2D brickWall_3;

		private Texture2D stoneWall;
		private Texture2D waterBody;

		private Texture2D coinPile;
		private Texture2D lifePack;

		private Texture2D rocketNorth;
		private Texture2D rocketEast;
		private Texture2D rockettSouth;
		private Texture2D rocketWest;

		private Texture2D gridMatrix;

		// Side screen contents
		private SpriteFont gameFont;
		private SpriteFont gameOverFont;
		private Texture2D whiteRectangle;


		//Screen properties
		private int screenWidth;
		private int screenHeight;

		//Time
		private double millisecondsPerFrame = 100; //Update every this amount of milliseconds
		private int lifeTimeReduce = 100;
		private double timeSinceLastUpdate = 0; //Accumulate the elapsed time

		//Ai
		private double millisecondsPerFrameAI = 2500; //Update every this amount of milliseconds
		private double timeCountAI = 0; //Accumulate the elapsed time

		private double millisecondsPerFrameBullet = 300; //Update every 300 milliseconds
		private double timeSinceLastUpdateBullet = 0; //Accumulate the elapsed time

		#endregion

		public GameScreen(ScreenManager game, SpriteBatch spriteBatch) : base(game, spriteBatch)
		{
			game.Content.RootDirectory = "Content";
			Initialize_Screen();
			imageRectangle = new Rectangle(0, 0, Game.Window.ClientBounds.Width, Game.Window.ClientBounds.Height);
        }

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		public void Initialize_Screen()
		{
			LoadContent();

			// TODO: Add your initialization logic here
			game.Window.Title = GameSettings.Title;
			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);
			primitiveBatch = new PrimitiveBatch(GraphicsDevice);

			// TODO: use this.Content to load your game content here
			screenWidth = game.GraphicsDevice.PresentationParameters.BackBufferWidth;
			screenHeight = game.GraphicsDevice.PresentationParameters.BackBufferHeight;

			backgroundTexture = game.Content.Load<Texture2D>("Images/background");

			playerTankNorth = game.Content.Load<Texture2D>("Images/tankNorth");
			playerTankEast = game.Content.Load<Texture2D>("Images/tankEast");
			playerTankSouth = game.Content.Load<Texture2D>("Images/tankSouth");
			playerTankWest = game.Content.Load<Texture2D>("Images/tankWest");

			empty = game.Content.Load<Texture2D>("Images/empty");

			brickWall_0 = game.Content.Load<Texture2D>("Images/brickwall_0");
			brickWall_1 = game.Content.Load<Texture2D>("Images/brickwall_1");
			brickWall_2 = game.Content.Load<Texture2D>("Images/brickwall_2");
			brickWall_3 = game.Content.Load<Texture2D>("Images/brickwall_3");

			stoneWall = game.Content.Load<Texture2D>("Images/stoneWall");

			coinPile = game.Content.Load<Texture2D>("Images/coinPile");
			lifePack = game.Content.Load<Texture2D>("Images/lifePack");

			rocketNorth = game.Content.Load<Texture2D>("Images/rocketNorth");
			rocketEast = game.Content.Load<Texture2D>("Images/rocketEast");
			rockettSouth = game.Content.Load<Texture2D>("Images/rocketSouth");
			rocketWest = game.Content.Load<Texture2D>("Images/rocketWest");

			waterBody = game.Content.Load<Texture2D>("Images/water");

			// Side screen items
			image = game.Content.Load<Texture2D>("tanksWallpaper");
			gameFont = game.Content.Load<SpriteFont>("fonts/controlFont");
			gameOverFont = game.Content.Load<SpriteFont>("fonts/gameOver");
			whiteRectangle = new Texture2D(GraphicsDevice, 1, 1);
			whiteRectangle.SetData(new[] { Color.White });

			gridMatrix = GenerateGrid(new Rectangle(0, 0, screenHeight, screenHeight), GameSettings.GridSize, GameSettings.GridSize, Color.LightGreen);
		}

		public void LoadGame()
		{
			gameEngine = new GameEngine();
			gameAi = new ConcreteAI(gameEngine.gameObjectHolder);

			if (gameEngine != null && gameEngine.StartClient(GameSettings.ServerIpAddress))
			{
				gameEngine.JoinToServer();
			}
		}

		public bool GameOnPlay()
		{
			return gameEngine != null;
		}

		public void UnLoadGame()
		{
			gameEngine.StopClient();
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// game-specific content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		/// <summary>
		/// Main Update method
		/// </summary>
		/// <param name="gameTime"></param>
		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
			keyboardState = Keyboard.GetState();
			if (keyboardState.IsKeyDown(Keys.Escape))
			{
				game.Exit();
			}

			GetAiNextMoveUpdate(gameTime);

			UpdateGameObjects(gameTime);
		}

		/// <summary>
		/// Main draw method
		/// </summary>
		/// <param name="gameTime"></param>
		public override void Draw(GameTime gameTime)
		{
			try
			{
				GraphicsDevice.Clear(new Color(10, 10, 0, 255));
				// TODO: Add your drawing code here
				spriteBatch.Begin();
				DrawScenery();
				DrawGridLines();
				DrawObstacles();
				DrawCoinPiles();
				DrawLifePacks();
				DrawPlayers();
				DrawBullets();
				DrawSideScreen();
				//
				if (gameEngine.gameObjectHolder.StateOfGame == ReplyTypes.GAME_FINISHED ||
					gameEngine.gameObjectHolder.StateOfGame == ReplyTypes.GAME_HAS_FINISHED ||
					gameEngine.gameObjectHolder.StateOfGame == ReplyTypes.DEAD ||
					gameEngine.gameObjectHolder.StateOfGame == ReplyTypes.PITFALL)
				{
					DrawGameFinished();
				}
				// Debug: DrawGameFinished();
			}
			catch (Exception ex)
			{
				Console.WriteLine("Draw() Exception : " + ex.Message + "\n" + ex.StackTrace);
			}
			finally
			{
				spriteBatch.End();
				base.Draw(gameTime);
			}
		}

		/// <summary>
		/// Manually control the tank
		/// </summary>
		/// <param name="keys"></param>
		/// <param name="keysPressed"></param>
		/// <param name="gameTime"></param>
		public override void UpdateScreen(Keys[] keys, Keys[] keysPressed, GameTime gameTime)
		{
			// TODO:
			if (keys.Contains(Keys.Left))
			{
				gameEngine.SendCommand(GameCommands.MoveLeft);
			}

			if (keys.Contains(Keys.Right))
			{
				gameEngine.SendCommand(GameCommands.MoveRight);
			}

			if (keys.Contains(Keys.Up))
			{
				gameEngine.SendCommand(GameCommands.MoveUp);
			}
			if (keys.Contains(Keys.Down))
			{
				gameEngine.SendCommand(GameCommands.MoveDown);
			}
			if (keys.Contains(Keys.Space))
			{
				gameEngine.SendCommand(GameCommands.Shoot);
			}
		}

		/// <summary>
		/// Get the next move from the AI
		/// </summary>
		/// <param name="gameTime"></param>
		private void GetAiNextMoveUpdate(GameTime gameTime)
		{

			//Update onece a update cycle
			timeCountAI += gameTime.ElapsedGameTime.TotalMilliseconds;
			if (timeCountAI >= millisecondsPerFrameAI)
			{
				timeCountAI = 0;
				String move = gameAi.getNextCommand();
                if (move == GameCommands.Shoot)
                {
                    millisecondsPerFrameAI = 1000;
                } else
                {
                    millisecondsPerFrameAI = 2500;
                }
				if (move != "")
					gameEngine.SendCommand(move);
			}
		}

		/// <summary>
		/// Update Coinpile lifetime
		/// </summary>
		/// <param name="gameTime"></param>
		private void UpdateGameObjects(GameTime gameTime)
		{
			Player[] players = new Player[5];
			Array.Copy(gameEngine.gameObjectHolder.Players, players, gameEngine.gameObjectHolder.Players.Count());

			//Update once a update cycle
			timeSinceLastUpdate += gameTime.ElapsedGameTime.TotalMilliseconds;

			if (timeSinceLastUpdate >= millisecondsPerFrame)
			{

				timeSinceLastUpdate = 0;

				//Update coinpile life
				//Only keep non expiered coin piles
				ConcurrentBag<CoinPile> newCoinPileBag = new ConcurrentBag<CoinPile>();
				foreach (CoinPile cp in gameEngine.gameObjectHolder.CoinPiles)
				{
					if (cp.LifeTime >= 0)
					{
						cp.LifeTime -= lifeTimeReduce;

						for (int i = 0; i < players.Count(); i++)
						{
							//player takes the coin pile
							if (players[i] != null && players[i].Health > 0 && players[i].Location == cp.Location)
							{
								cp.LifeTime = -lifeTimeReduce;
								break;
							}
						}
						//Add coin pile to new bag
						if (cp.LifeTime > 0)
						{
							newCoinPileBag.Add(cp);
						}
					}

				}
				//Replace old coin pile bag
				gameEngine.gameObjectHolder.CoinPiles = newCoinPileBag;


				//Update lifePacks
				//Only keep non expiered life packs
				ConcurrentBag<LifePack> newLifePackBag = new ConcurrentBag<LifePack>();
				foreach (LifePack lp in gameEngine.gameObjectHolder.LifePacks)
				{
					if (lp.LifeTime >= 0)
					{
						lp.LifeTime -= lifeTimeReduce;

						//player takes lifepack
						for (int i = 0; i < players.Count(); i++)
						{
							if (players[i] != null && players[i].Health > 0 && players[i].Location == lp.Location)
							{
								lp.LifeTime = -lifeTimeReduce;
								break;
							}
						}
						//Add life pack to new bag
						if (lp.LifeTime > 0)
						{
							newLifePackBag.Add(lp);
						}
					}
				}
				//Replace old life pack bag
				gameEngine.gameObjectHolder.LifePacks = newLifePackBag;

			}

			//Update 1/3 of a second for bullets
			timeSinceLastUpdateBullet += gameTime.ElapsedGameTime.TotalMilliseconds;
			if (timeSinceLastUpdateBullet >= millisecondsPerFrameBullet)
			{
				timeSinceLastUpdateBullet = 0;
				//Update bullets
				//Only keep non expiered life packs
				ConcurrentBag<Bullet> newBulletBag = new ConcurrentBag<Bullet>();
				foreach (Bullet bullet in gameEngine.gameObjectHolder.Bullets)
				{

					//Check if bullet hit something
					if (bullet.IsLive)
					{
						//See if bullet hit a wall
						//Hit a  stone wall
						List<Point> stoneWalls = gameEngine.gameObjectHolder.StoneWalls.ToList();
						for (int i = 0; i < stoneWalls.Count; i++)
						{
							if (DetectBulletImpact(bullet, stoneWalls[i]))
							{
								bullet.IsLive = false;
								break;
							}

						}

						if (bullet.IsLive)
						{
							//Hit a brick wall
							Hashtable brickWalls = Hashtable.Synchronized(gameEngine.gameObjectHolder.BrickWalls);
							foreach (DictionaryEntry entry in brickWalls)
							{
								if ((int)entry.Value < 4 && DetectBulletImpact(bullet, (Point)entry.Key))
								{
									bullet.IsLive = false;
									break;
								}
							}
						}
						if (bullet.IsLive)
						{
							//Hit a player
							for (int i = 0; i < players.Count(); i++)
							{

								if (players[i] != null && players[i].Health > 0 && DetectBulletImpact(bullet, players[i].Location))
								{
									bullet.IsLive = false;
									break;
								}
							}
						}

					}

					//If bullet is travelling
					if (bullet.IsLive)
					{
						int limitUp = 0;
						int limitDown = GameSettings.GridSize;
						int limitLeft = 0;
						int limitRight = GameSettings.GridSize;

						if (bullet.Direction == Directions.North)
						{
							int locationY = bullet.Location.Y - 1;
							if (locationY < limitUp)
							{
								bullet.IsLive = false;
							}
							else
							{
								Point newLoc = new Point(bullet.Location.X, locationY);
								bullet.Location = newLoc;
							}
						}
						else if (bullet.Direction == Directions.East)
						{
							int locationX = bullet.Location.X + 1;
							if (locationX >= limitRight)
							{
								bullet.IsLive = false;
							}
							else
							{
								Point newLoc = new Point(locationX, bullet.Location.Y);
								bullet.Location = newLoc;
							}
						}
						else if (bullet.Direction == Directions.South)
						{
							int locationY = bullet.Location.Y + 1;
							if (locationY >= limitDown)
							{
								bullet.IsLive = false;
							}
							else
							{
								Point newLoc = new Point(bullet.Location.X, locationY);
								bullet.Location = newLoc;
							}
						}
						else if (bullet.Direction == Directions.West)
						{
							int locationX = bullet.Location.X - 1;
							if (locationX < limitLeft)
							{
								bullet.IsLive = false;
							}
							else
							{
								Point newLoc = new Point(locationX, bullet.Location.Y);
								bullet.Location = newLoc;
							}
						}

						//Add bullet to new bag
						if (bullet.IsLive)
						{
							newBulletBag.Add(bullet);
						}

					}
				}

				//Replace old bullet bag
				gameEngine.gameObjectHolder.Bullets = newBulletBag;
			}
		}

		#region Draw

		/// <summary>
		/// Draw map obstacles
		/// </summary>
		private void DrawObstacles()
		{
			//Load brick walls
			Hashtable brickWalls = Hashtable.Synchronized(gameEngine.gameObjectHolder.BrickWalls);
			foreach (DictionaryEntry entry in brickWalls)
			{
				Texture2D brickWall = empty;
				if ((int)entry.Value == 0)
				{
					brickWall = brickWall_0;
				}
				else if ((int)entry.Value == 1)
				{

					brickWall = brickWall_1;
				}
				else if ((int)entry.Value == 2)
				{
					brickWall = brickWall_2;
				}
				else if ((int)entry.Value == 3)
				{

					brickWall = brickWall_3;
				}

				DrawBrickWall(brickWall, (GameSettings.TileSize * ((Point)entry.Key).X), (GameSettings.TileSize * ((Point)entry.Key).Y));
			}


			//Load stone wals
			List<Point> stoneWalls = gameEngine.gameObjectHolder.StoneWalls.ToList();

			for (var i = 0; i < stoneWalls.Count; i++)
			{
				DrawStoneWall(0 + (GameSettings.TileSize * stoneWalls[i].X), 0 + (GameSettings.TileSize * stoneWalls[i].Y));

			}

			//Load water bodies
			List<Point> waterBodies = gameEngine.gameObjectHolder.WaterBodies.ToList();
			for (var i = 0; i < waterBodies.Count; i++)
			{
				DrawWaterBody(0 + (GameSettings.TileSize * waterBodies[i].X), 0 + (GameSettings.TileSize * waterBodies[i].Y));

			}
		}

		/// <summary>
		/// Draw coin piles
		/// </summary>
		private void DrawCoinPiles()
		{
			//Load coin piles
			List<CoinPile> coinPiles = gameEngine.gameObjectHolder.CoinPiles.ToList();

			foreach (CoinPile cp in coinPiles)
			{
				if (cp.LifeTime > 0)
				{
					DrawCoinPile((GameSettings.TileSize * cp.Location.X), (GameSettings.TileSize * cp.Location.Y));
				}
			}
		}

		/// <summary>
		/// Draw life packs
		/// </summary>
		private void DrawLifePacks()
		{
			//Load coin piles
			List<LifePack> lifePacks = gameEngine.gameObjectHolder.LifePacks.ToList();

			foreach (LifePack lp in lifePacks)
			{
				if (lp.LifeTime > 0)
				{
					DrawLifePack((GameSettings.TileSize * lp.Location.X), (GameSettings.TileSize * lp.Location.Y));
				}
			}
		}

		/// <summary>
		/// Draw bullets
		/// </summary>
		private void DrawBullets()
		{
			foreach (Bullet bullet in gameEngine.gameObjectHolder.Bullets)
			{
				if (bullet.IsLive)
				{
					Texture2D bulletTexture2D = rocketNorth;
					if (bullet.Direction == Directions.East)
					{
						bulletTexture2D = rocketEast;
					}
					else if (bullet.Direction == Directions.South)
					{
						bulletTexture2D = rockettSouth;
					}
					else if (bullet.Direction == Directions.West)
					{
						bulletTexture2D = rocketWest;
					}

					DrawBullet(bulletTexture2D, (GameSettings.TileSize * bullet.Location.X), (GameSettings.TileSize * bullet.Location.Y));
				}
			}
		}

		/// <summary>
		/// Draw players
		/// </summary>
		private void DrawPlayers()
		{
			try
			{
				Player[] players = new Player[5];
				Color[] colors = { Color.Green, Color.Red, Color.Blue, Color.Yellow, Color.Purple };
				Array.Copy(gameEngine.gameObjectHolder.Players, players, gameEngine.gameObjectHolder.Players.Count());

				for (int i = 0; i < players.Count(); i++)
				{
					if (players[i] != null && players[i].Health > 0)
					{
						Player p = players[i];
						Texture2D playerTank = playerTankNorth;
						if (p.Direction == Directions.East)
						{
							playerTank = playerTankEast;
						}
						else if (p.Direction == Directions.South)
						{
							playerTank = playerTankSouth;
						}
						else if (p.Direction == Directions.West)
						{
							playerTank = playerTankWest;
						}

						int speed = 1;

						int x = GameSettings.TileSize * p.Location.X;
						int y = GameSettings.TileSize * p.Location.Y;

						int dx = Math.Abs(x - p.Position.X);
						if (dx >= speed)
						{
							x = p.Position.X + speed * (x - p.Position.X) / dx;
						}

						int dy = Math.Abs(y - p.Position.Y);
						if (dy >= speed)
						{
							y = p.Position.Y + speed * (y - p.Position.Y) / dy;
						}

						p.Position = new Point(x, y);

						DrawTank(playerTank, colors[i], x, y);
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("DrawPlayers() Exception : " + ex.Message + "\n" + ex.StackTrace);
			}

		}


		/// <summary>
		/// Draw game background
		/// </summary>
		private void DrawScenery()
		{
			Rectangle screenRectangle = new Rectangle(0, 0, screenHeight, screenHeight);
			// spriteBatch.Draw(image, imageRectangle, Color.White);
			spriteBatch.Draw(backgroundTexture, screenRectangle, Color.White);
		}

		/// <summary>
		/// Draw 2d grid lines
		/// </summary>
		private void DrawGridLines()
		{
			Rectangle screenRectangle = new Rectangle(0, 0, screenWidth, screenHeight);
			spriteBatch.Draw(gridMatrix, screenRectangle, Color.White);
		}

		/// <summary>
		/// Draw tank
		/// </summary>
		/// <param name="playerColor"></param>
		/// <param name="posX"></param>
		/// <param name="posY"></param>
		private void DrawTank(Texture2D playerTank, Color playerColor, int posX, int posY)
		{
			Rectangle screenRectangle = new Rectangle(posX, posY, GameSettings.TileSize, GameSettings.TileSize);
			spriteBatch.Draw(playerTank, screenRectangle, playerColor);
		}

		/// <summary>
		/// Draw beick wall
		/// </summary>
		/// <param name="brickWallTexture"></param>
		/// <param name="posX"></param>
		/// <param name="posY"></param>
		private void DrawBrickWall(Texture2D brickWallTexture, int posX, int posY)
		{
			Rectangle screenRectangle = new Rectangle(posX, posY, GameSettings.TileSize, GameSettings.TileSize);
			spriteBatch.Draw(brickWallTexture, screenRectangle, Color.White);
		}

		/// <summary>
		/// Draw stone wall
		/// </summary>
		/// <param name="posX"></param>
		/// <param name="posY"></param>
		private void DrawStoneWall(int posX, int posY)
		{
			Rectangle screenRectangle = new Rectangle(posX, posY, GameSettings.TileSize, GameSettings.TileSize);
			spriteBatch.Draw(stoneWall, screenRectangle, Color.White);
		}

		/// <summary>
		/// Draw water body
		/// </summary>
		/// <param name="posX"></param>
		/// <param name="posY"></param>
		private void DrawWaterBody(int posX, int posY)
		{
			Rectangle screenRectangle = new Rectangle(posX, posY, GameSettings.TileSize, GameSettings.TileSize);
			spriteBatch.Draw(waterBody, screenRectangle, Color.White);
		}

		/// <summary>
		/// Draw coin pile
		/// </summary>
		/// <param name="posX"></param>
		/// <param name="posY"></param>
		private void DrawLifePack(int posX, int posY)
		{
			Rectangle screenRectangle = new Rectangle(posX, posY, GameSettings.TileSize, GameSettings.TileSize);
			spriteBatch.Draw(lifePack, screenRectangle, Color.White);
		}

		/// <summary>
		/// Draw bullet
		/// </summary>
		/// <param name="bulletTexture2D"></param>
		/// <param name="posX"></param>
		/// <param name="posY"></param>
		private void DrawBullet(Texture2D bulletTexture2D, int posX, int posY)
		{

			Rectangle screenRectangle = new Rectangle(posX, posY, GameSettings.TileSize, GameSettings.TileSize);
			spriteBatch.Draw(bulletTexture2D, screenRectangle, Color.White);
		}

		/// <summary>
		/// Draw life pack
		/// </summary>
		/// <param name="posX"></param>
		/// <param name="posY"></param>
		private void DrawCoinPile(int posX, int posY)
		{

			Rectangle screenRectangle = new Rectangle(posX, posY, GameSettings.TileSize, GameSettings.TileSize);
			spriteBatch.Draw(coinPile, screenRectangle, Color.White);
		}

		/// <summary>
		/// Draws side contents
		/// </summary>
		private void DrawSideScreen()
		{
			if (GameSettings.ReslutionWidth - GameSettings.ReslutionHeight > 0)
			{
				Color[] colors = { Color.Green, Color.Red, Color.Blue, Color.Yellow, Color.Purple };
				Rectangle screenRectangle = new Rectangle(GameSettings.ReslutionHeight, 0,
					GameSettings.ReslutionWidth - GameSettings.ReslutionHeight, GameSettings.ReslutionHeight);
				Vector2 loc = screenRectangle.Location.ToVector2() + new Vector2(20, 20);
				// Draw if only content exixt
				try
				{
					// Draw the Life bar
					Player[] players = gameEngine.gameObjectHolder.Players;
					int playerId = gameEngine.gameObjectHolder.PlayerId;
					if (playerId < players.Length && playerId >= 0)
					{
						Player player = players[playerId];
						if (player != null)
                        {
                            spriteBatch.DrawString(gameFont, "TechTank: P" + playerId, loc, Color.White);
                            loc += new Vector2(0, gameFont.LineSpacing);
                            spriteBatch.Draw(playerTankEast, new Rectangle(loc.ToPoint(), new Point(90, 90)), colors[playerId]);

							// Draw health bar
							loc += new Vector2(95, 0);
							int health = player.Health;
							spriteBatch.DrawString(gameFont, "Health: " + player.Health.ToString(), loc, Color.White);
							loc += new Vector2(0, 25);
							DrawLifeBar(loc.ToPoint(), 150, health);

							// Draw coin count
							loc += new Vector2(0, 20);
							spriteBatch.DrawString(gameFont, "Coins: " + player.Coins.ToString(), loc, Color.White);

							// Draw point count
							loc += new Vector2(0, 20);
							spriteBatch.DrawString(gameFont, "Points: " + player.Points.ToString(), loc, Color.White);

							// Draw other player details
							loc += new Vector2(-95, 35);
							for (int i = 0; i < players.Length; i++)
							{
								if (i != playerId && players[i] != null)
								{
                                    spriteBatch.Draw(playerTankEast, new Rectangle(loc.ToPoint(), new Point(40, 40)), colors[i]);
                                    loc += new Vector2(0, gameFont.LineSpacing / 3);
                                    spriteBatch.DrawString(gameFont, "  P" + i, loc, Color.White);
                                    loc += new Vector2(0, -(gameFont.LineSpacing / 2));
                                    player = players[i];

									// Draw coin count
									loc += new Vector2(50, 0);
									spriteBatch.DrawString(gameFont, "Health: " + player.Health.ToString(), loc, Color.White);

									// Draw coin count
									loc += new Vector2(0, 20);
									spriteBatch.DrawString(gameFont, "Coins: " + player.Coins.ToString(), loc, Color.White);

									// Draw point count
									loc += new Vector2(0, 20);
									spriteBatch.DrawString(gameFont, "Points: " + player.Points.ToString(), loc, Color.White);

									loc += new Vector2(-50, 30);
								}
							}
						}
					}
				}
				catch (Exception) { /* Do nothing */ }

			}

		}

		/// <summary>
		/// Draws lifebar fo sidebar
		/// </summary>
		/// <param name="loc"></param>
		/// <param name="width"></param>
		/// <param name="health"></param>
		private void DrawLifeBar(Point loc, int width, int health)
		{
			Rectangle baseRect = new Rectangle(loc, new Point(width, 20));
			spriteBatch.Draw(whiteRectangle, baseRect, Color.White);
            if (health > 150) health = 150;
			baseRect.Width = health * width / 100;
			Color lifebarColor = Color.Green;
			if (health < 10)
				lifebarColor = Color.DarkRed;
			else if (health < 30)
				lifebarColor = Color.OrangeRed;
			else if (health < 50)
				lifebarColor = Color.Orange;
			else if (health < 60)
				lifebarColor = Color.MonoGameOrange;
			else if (health < 75)
				lifebarColor = Color.Yellow;
            else if (health > 150)
                lifebarColor = Color.Blue;
            spriteBatch.Draw(whiteRectangle, baseRect, lifebarColor);
		}

		/// <summary>
		/// Drawn when game is finished
		/// </summary>
		private void DrawGameFinished()
		{
			Rectangle baseRect = new Rectangle(new Point(50, 100), new Point(GameSettings.ReslutionHeight - 100, GameSettings.ReslutionHeight - 300));
			spriteBatch.Draw(whiteRectangle, baseRect, Color.Brown);
			Vector2 locGameOver = new Vector2((GameSettings.ReslutionHeight - 100) / 2 - 20, 120);
			Vector2 locMyStatus = new Vector2((GameSettings.ReslutionHeight - 100) / 2 - 80, 155);
			spriteBatch.DrawString(gameOverFont, "Game Over", locGameOver, Color.Black);
			bool won = true;
			if (gameEngine.gameObjectHolder.PlayerId >= 0)
			{
				Player me = gameEngine.gameObjectHolder.Players[gameEngine.gameObjectHolder.PlayerId];
				if (me.Health <= 0)
				{
					won = false;
				}
				else
				{
					foreach (Player player in (gameEngine.gameObjectHolder.Players))
					{
						if (player != null && player.Points > me.Points)
						{
							won = false;
							break;
						}
					}
				}
				if (won)
				{
					spriteBatch.DrawString(gameOverFont, "You Won !! Good Job :)", locMyStatus, Color.Black);
					spriteBatch.DrawString(gameOverFont, "Your Point count: " + me.Points, locMyStatus + new Vector2(5, 35), Color.Gold);
				}
				else
				{
					spriteBatch.DrawString(gameOverFont, "You lost. Try Again :(", locMyStatus, Color.Black);
				}
			}
		}

		#endregion

		#region Helper methods 

		/// <summary>
		/// Used to generate the grid lines
		/// </summary>
		/// <param name="destRect">The container rectangle</param>
		/// <param name="cols">No of columns</param>
		/// <param name="rows">No of rows</param>
		/// <param name="gridColor">Color of the grid lines</param>
		/// <returns></returns>
		private Texture2D GenerateGrid(Rectangle destRect, int cols, int rows, Color gridColor)
		{
			RenderTarget2D grid = new RenderTarget2D(GraphicsDevice, destRect.Width,
				destRect.Height);
			try
			{
				float gridCellWidth = destRect.Width / (float)cols;
				float gridCellHight = destRect.Height / (float)rows;
				int w = (int)(cols * gridCellWidth);
				int h = (int)(rows * gridCellHight);

				float uselessWidth = destRect.Width - w;
				float uselessHight = destRect.Height - h;

				Rectangle bounds = new Rectangle((int)(uselessWidth / 2) + destRect.X, (int)(uselessHight / 2) + destRect.Y, w, h);

				GraphicsDevice.SetRenderTarget(grid);
				GraphicsDevice.Clear(Color.Transparent);

				primitiveBatch.Begin(PrimitiveType.LineList);

				float x = bounds.X;
				float y = bounds.Y;

				for (int col = 0; col < cols + 1; col++)
				{
					primitiveBatch.AddVertex(new Vector2(x + (col * gridCellWidth), bounds.Top), gridColor);
					primitiveBatch.AddVertex(new Vector2(x + (col * gridCellWidth), bounds.Bottom), gridColor);
				}

				for (int row = 0; row < rows + 1; row++)
				{
					primitiveBatch.AddVertex(new Vector2(bounds.Left, y + (row * gridCellHight)), gridColor);
					primitiveBatch.AddVertex(new Vector2(bounds.Right, y + (row * gridCellHight)), gridColor);
				}
				primitiveBatch.End();

				GraphicsDevice.SetRenderTarget(null);

			}
			catch (Exception ex)
			{
				Console.WriteLine("GenerateGrid() Exception : " + ex.Message);
			}
			return grid;
		}

		/// <summary>
		///  Check if a bullet collides with an obstacle
		/// </summary>
		/// <param name="bullet">Bullet to test</param>
		/// <param name="obstacleLocation">testing obstacle location</param>
		/// <returns></returns>
		private bool DetectBulletImpact(Bullet bullet, Point obstacleLocation)
		{
			return (bullet.Direction == Directions.North && bullet.Location.X == obstacleLocation.X && bullet.Location.Y - 1 == obstacleLocation.Y) ||
				   (bullet.Direction == Directions.East && bullet.Location.Y == obstacleLocation.Y && bullet.Location.X + 1 == obstacleLocation.X) ||
				   (bullet.Direction == Directions.South && bullet.Location.X == obstacleLocation.X && bullet.Location.Y + 1 == obstacleLocation.Y) ||
				   (bullet.Direction == Directions.West && bullet.Location.Y == obstacleLocation.Y && bullet.Location.X - 1 == obstacleLocation.X) ||

				   (bullet.Location == obstacleLocation);
		}

		#endregion
	}
}