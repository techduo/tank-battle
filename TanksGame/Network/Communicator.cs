﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace TanksGame.Network
{
    public delegate void MessageRecievedEventHandler(Object obj, MessageRecievedEventArgs e);

    /// <summary>
    /// Class responsible for Communication handeling
    /// </summary>
    class Communicator
    {

        #region Declerations			

        private IPAddress serverIp = null;
        #endregion

        #region Variables
        //Stream - outgoing
        private NetworkStream clientStream;
        //To talk back to the client
        private TcpClient client;
        //To write to the clients
        private BinaryWriter writer;

        //Stream - incoming 
        private NetworkStream serverStream;
        //To listen to the clinets
        private TcpListener listener;
        //The message to be written       
        public string lastReply = String.Empty;

        private Thread thread;
        private Boolean isListening;

        /// <summary>
        /// Singleton Communicator - hence private
        /// </summary>
        private static Communicator comm = new Communicator();
        #endregion

        #region Events
        /// <summary>
        /// Event raised when new message is recieved from the server
        /// </summary>
        public event MessageRecievedEventHandler MessageRecieved;
        #endregion

        #region Const & Singleton

        /// <summary>
        /// Do not instantiate 
        /// </summary>
        private Communicator()
        {
        }

        public static Communicator GetInstance()
        {
            return comm;
        }
        #endregion

        #region Listen To Server Methods
        /// <summary>
        /// Start listening to the server
        /// </summary>
        /// <returns>Whether Started listening or not</returns>
        public Boolean StartListning(string serverIp)
        {
            try
            {
                this.serverIp = IPAddress.Parse(serverIp);
                if (isListening == false)
                {
                    thread = new Thread(new ThreadStart(ReceiveData));
                    thread.Start();
                    isListening = true;
                    return true;
                }
                else
                {
                    Console.WriteLine("ERROR\t: " + "The logger is currently listening to a server, Disconnect from it first.");
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR\t: " + e.Message);
                Console.WriteLine(e.Data);
                return false;
            }
        }

        /// <summary>
        /// Stop listening to the server
        /// </summary>
        public void StopListning()
        {
            isListening = false;
        }

        /// <summary>
        /// Recieve Data while isListening is True
        /// </summary>
        private void ReceiveData()
        {
            bool errorOcurred = false;
            //The socket that is listened to 
            Socket connection = null;
            try
            {
                //Creating listening Socket
                this.listener = new TcpListener(IPAddress.Any, 7000);
                //Starts listening
                this.listener.Start();
                //Establish connection upon client request
                while (isListening)
                {
                    if (listener.Pending())
                    {
                        // Connected socket
                        connection = listener.AcceptSocket();
                        if (connection.Connected)
                        {
                            //To read from socket create NetworkStream object associated with socket
                            this.serverStream = new NetworkStream(connection);

                            SocketAddress sockAdd = connection.RemoteEndPoint.Serialize();
                            string s = connection.RemoteEndPoint.ToString();
                            List<Byte> inputStr = new List<byte>();

                            int asw = 0;
                            while (asw != -1)
                            {
                                asw = this.serverStream.ReadByte();
                                inputStr.Add((Byte)asw);
                            }

                            lastReply = Encoding.UTF8.GetString(inputStr.ToArray());
                            this.serverStream.Close();
                            Console.WriteLine("READ\t: " + lastReply.Substring(0, lastReply.Length - 1));
                            // Relay message to every Observer
                            try
                            {
                                MessageRecievedEventArgs msgEvtArgs = new MessageRecievedEventArgs(lastReply.Substring(0, lastReply.Length - 1), DateTime.Now);
                                MessageRecieved.Invoke(null, msgEvtArgs);
                            }
                            catch (Exception)
                            { // Igonred 
                            }
                        }
                        // listener.Stop();
                    }
                }
                thread.Abort();
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR\t: " + e.Message);
                if (isListening) errorOcurred = true;
            }
            finally
            {
                if (connection != null)
                    if (connection.Connected)
                        connection.Close();
                if (errorOcurred)
                    this.ReceiveData();
            }
        }
        #endregion

        #region Write To Server Methods
        /// <summary>
        /// Send data to the server
        /// </summary>
        /// <param name="msg"></param>
        public void SendData(String msg)
        {
            //Opening the connection
            this.client = new TcpClient();

            try
            {
                this.client.Connect(serverIp, 6000);

                if (this.client.Connected)
                {
                    //To write to the socket
                    this.clientStream = client.GetStream();

                    //Create objects for writing across stream
                    this.writer = new BinaryWriter(clientStream);
                    Byte[] tempStr = Encoding.ASCII.GetBytes(msg);

                    //writing to the port                
                    this.writer.Write(tempStr);
                    Console.WriteLine("WRITE\t: " + msg + " is written to Server on Port: 6000");
                    this.writer.Close();
                    this.clientStream.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR\t: " + e.Message);
            }
            finally
            {
                this.client.Close();
            }
        }
        #endregion


    }
}
