﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TanksGame.Network
{
	public class MessageRecievedEventArgs : EventArgs
	{
		/// <summary>
		/// Message recieved
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Time the message recieved
		/// </summary>
		public DateTime Time { get; set; }

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message">Message</param>
		/// <param name="time">Time</param>
		public MessageRecievedEventArgs(string message, DateTime time)
		{
			Message = message;
			Time = time;
		}
	}
}
