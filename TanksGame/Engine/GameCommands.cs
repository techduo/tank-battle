﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TanksGame.Engine
{
    /// <summary>
    ///list of accepted commands by server
    /// </summary>
    public class GameCommands
    {
        public const string MoveLeft = "LEFT#";
        public const string MoveRight = "RIGHT#";
        public const string MoveUp = "UP#";
        public const string MoveDown = "DOWN#";
        public const string Shoot = "SHOOT#";
        public const string Join = "JOIN#";
    }
}