﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TanksGame.Network;
using TanksGame.Objects.ReplyObjects;
using TanksGame.Syntax;
using Microsoft.Xna.Framework;

namespace TanksGame.Engine
{
    public class GameEngine
    {
        #region Declerations

        private bool ServerConnected = false;

        //Game objects
        public GameObjectHolder gameObjectHolder { get; }

        #endregion

        #region Constructor
        public GameEngine()
        {
            gameObjectHolder = new GameObjectHolder();
        }
        #endregion

        #region Helper methods

        /// <summary>
        /// Check if an ip is ipv4 compatible
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static bool IsIPv4(string value)
        {
            var quads = value.Split('.');
            // if we do not have 4 quads, return false
            if (quads.Length != 4)
                return false;
            // for each quad
            foreach (var quad in quads)
            {
                int q;
                // if parse fails 
                // or length of parsed int != length of quad string (i.e.; '1' vs '001')
                // or parsed int < 0
                // or parsed int > 255
                // return false
                if (!Int32.TryParse(quad, out q)
                    || !q.ToString().Length.Equals(quad.Length)
                    || q < 0
                    || q > 255)
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Server communication

        /// <summary>
        /// Establish connection to server
        /// </summary>
        public bool StartClient(String serverIpAddress)
        {
            Console.WriteLine("Game Engine :Starting client");
            if (serverIpAddress == "") return false;
            if (!IsIPv4(serverIpAddress))
            {
                Console.WriteLine("Invalid IP Address");
                return false;
            }
            Console.WriteLine("Attempting establish connection to : " + serverIpAddress);
            bool v = Communicator.GetInstance().StartListning(serverIpAddress);
            if (v)
            {
                ServerConnected = true;
                Communicator.GetInstance().MessageRecieved += Remote_MessageRecieved;//Subscribe to communicator to recieve server msgs
                Console.WriteLine("Attempt sucess");
                return true;
            }
            else
            {
                ServerConnected = false;
                return false;
            }
        }

        public void StopClient()
        {
            Communicator.GetInstance().StopListning();
        }

        /// <summary>
        /// Connect to server game
        /// </summary>
        public void JoinToServer()
        {
            SendCommand(GameCommands.Join);
        }

        /// <summary>
        /// Send command to server
        /// Use values in GameCommands class
        /// </summary>
        /// <param name="command">GameCommand</param>
        public void SendCommand(string command)
        {
            Communicator.GetInstance().SendData(command);
        }

        /// <summary>
        /// This is invoked by the communicator thread
        /// Hence a deligate is required
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private void Remote_MessageRecieved(object obj, MessageRecievedEventArgs e)
        {

            this.UpdateGameObjects(e);
        }

        #endregion

        #region  Update Game Objects

        /// <summary>
        /// Output to console
        /// </summary>
        /// <param name="s"></param>
        public void ShowOnTextbox(string s = "")
        {
            Console.WriteLine(s);
        }

        /// <summary>
        /// Show the recieved msg
        /// </summary>
        /// <param name="e"></param>
        private void UpdateGameObjects(MessageRecievedEventArgs e)
        {
            ShowOnTextbox();
            ShowOnTextbox("--- PARSED_INFO ---");
            ShowOnTextbox();

            Reply r = Parser.GetInstance.Parse(e.Message);

            //Load last player states
            Player[] oldPlayers = new Player[5];
            Array.Copy(gameObjectHolder.Players, oldPlayers, gameObjectHolder.Players.Count());

            switch (r.MessageType)
            {
                case ReplyTypes.GAME_START:
                case ReplyTypes.PLAYERS_FULL:
                case ReplyTypes.ALREADY_ADDED:
                case ReplyTypes.GAME_ALREADY_STARTED:
                case ReplyTypes.DEAD:
                case ReplyTypes.GAME_HAS_FINISHED:
                case ReplyTypes.GAME_NOT_STARTED_YET:
                case ReplyTypes.GAME_FINISHED:
                case ReplyTypes.PITFALL:
                    gameObjectHolder.StateOfGame = r.MessageType;
                    break;
            }

            ShowOnTextbox("| Reply type: " + r.MessageType + " |");

            ShowOnTextbox();

            if (r.Data is CoinPile)
            {
                CoinPile cp = (CoinPile)r.Data;

                //Add coinpile to coinpile array
                gameObjectHolder.CoinPiles.Add(cp);

            }
            else if (r.Data is GameVariableUpdate)
            {

                GameVariableUpdate gvu = (GameVariableUpdate)r.Data;

                List<Player> players = gvu.Players;
                foreach (Player p in players)
                {

                    //Update player array

                    //If player is dead add a coinpile
                    if (p.Health <= 0 && 0 < oldPlayers[p.PlayerID].Health)
                    {

                        bool isDeadByEnv = false;
                        List<Point> waterBodies = gameObjectHolder.WaterBodies.ToList();

                        //Load water bodies
                        for (var i = 0; i < waterBodies.Count; i++)
                        {
                            if (p.Location == waterBodies[i])
                            {
                                isDeadByEnv = true;
                                break;
                            }
                        }

                        //if player was not killed by water then add coin pile
                        if (!isDeadByEnv)
                        {
                            CoinPile cp = new CoinPile
                            {
                                Location = p.Location,
                                Value = p.Coins,
                                LifeTime = long.MaxValue
                            };

                            gameObjectHolder.CoinPiles.Add(cp);
                        }
                    }

                    Player pOld = gameObjectHolder.Players[p.PlayerID];
                    if (pOld != null)
                    {
                        p.Rotation = pOld.Rotation;
                        p.Position = pOld.Position;
                    }
                    else if (pOld == null)
                    {
                        p.Position = new Point(p.Location.X * GameSettings.GridSize, p.Location.Y * GameSettings.GridSize);
                        p.Rotation = Player.DirectionToRadians(p.Direction);
                    }

					//Update player
                    gameObjectHolder.Players[p.PlayerID] = p;

                    if (p.Shot == 1)
                    {
                        Bullet bullet = new Bullet();
                        if (p.Direction == Directions.North)
                        {
                            bullet.Location = new Point(p.Location.X, p.Location.Y - 1);
                        }
                        else if (p.Direction == Directions.East)
                        {
                            bullet.Location = new Point(p.Location.X + 1, p.Location.Y);
                        }
                        else if (p.Direction == Directions.South)
                        {
                            bullet.Location = new Point(p.Location.X, p.Location.Y + 1);

                        }
                        else if (p.Direction == Directions.West)
                        {
                            bullet.Location = new Point(p.Location.X - 1, p.Location.Y);
                        }

                        bullet.Direction = p.Direction;
                        bullet.IsLive = true;
                        gameObjectHolder.Bullets.Add(bullet);
                    }

                }

                List<BrickWallDamageReport> brickWallDamage = gvu.BrickWallDamage;
                foreach (BrickWallDamageReport bwdr in brickWallDamage)
                {
                    //Update brickwalls
                    //update brickwall damage as  bwdr.Damage
                    if (gameObjectHolder.BrickWalls.Contains(bwdr.Location))
                    {
                        gameObjectHolder.BrickWalls[bwdr.Location] = bwdr.Damage;
                    }
                    else
                    {
                        throw new Exception("No such BrickWall");
                    }

                }

            }
            else if (r.Data is LifePack)
            {
                LifePack lp = (LifePack)r.Data;

                //Add life pack to array 
                gameObjectHolder.LifePacks.Add(lp);

            }
            else if (r.Data is MapObstacle)
            {
                MapObstacle mo = (MapObstacle)r.Data;
                List<Point> brickWalls = mo.BrickWalls;
                List<Point> stoneWalls = mo.StoneWalls;
                List<Point> waterBlocks = mo.WaterBlocks;


                //ShowOnTextbox("BrickWalls :");
                foreach (Point pt in brickWalls)
                {

                    //Add brickwall coordinates and initial damage as 0
                    gameObjectHolder.BrickWalls.Add(pt, 0);
                }

                //initialize obstacle array
                gameObjectHolder.StoneWalls = stoneWalls;
                gameObjectHolder.WaterBodies = waterBlocks;

                //Set client id
                gameObjectHolder.PlayerId = mo.ClientPlayerID;
            }
            else if (r.Data is GameStartInfo)
            {

                GameStartInfo gsi = (GameStartInfo)r.Data;
                List<Player> players = gsi.Players;
                foreach (Player p in players)
                {
                    //Add player to fixed player array
                    Player pOld = gameObjectHolder.Players[p.PlayerID];
                    if (p != null && pOld != null)
                    {
                        p.Rotation = pOld.Rotation;
                        p.Position = pOld.Position;
                    }
                    else if (p != null && pOld == null)
                    {
                        p.Position = new Point(p.Location.X * GameSettings.TileSize, p.Location.Y * GameSettings.TileSize);
                        p.Rotation = Player.DirectionToRadians(p.Direction);
                    }
                    gameObjectHolder.Players[p.PlayerID] = p;
                }
            }
        }
    }
    #endregion

}
