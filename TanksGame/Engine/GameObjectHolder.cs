﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TanksGame.Objects.ReplyObjects;
//using System.Drawing;
using Microsoft.Xna.Framework;
using System.Collections.Concurrent;

namespace TanksGame.Engine
{
	public class GameObjectHolder
	{

		//Game state 
		public ReplyTypes StateOfGame { get; set; }

		//Player id
		public int PlayerId { get; set; }

		//Players
		public Player[] Players { get; }

        //Coinpiles
        public ConcurrentBag<CoinPile> CoinPiles { get; set; }

        //LifePacks
        public ConcurrentBag<LifePack> LifePacks { get; set; }

        //Bullets
        public ConcurrentBag<Bullet> Bullets { get; set; }

        //Brick walls
        public Hashtable BrickWalls { get; }

		//Stone walls
		public List<Point> StoneWalls { get; set; }

		//Water body
		public List<Point> WaterBodies { get; set; }

        /// <summary>
        /// Constructor initialises the object holders
        /// </summary>
        public GameObjectHolder()
		{
			StateOfGame=ReplyTypes.NOT_SET;
			PlayerId = -1;
			Players = new Player[5];
            CoinPiles = new ConcurrentBag<CoinPile>();
            LifePacks = new ConcurrentBag<LifePack>();
            Bullets = new ConcurrentBag<Bullet>();
            BrickWalls = new Hashtable();
			StoneWalls = new List<Point>();
			WaterBodies = new List<Point>();
		}

	}

}
