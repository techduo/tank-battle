﻿using Microsoft.Xna.Framework;

namespace TanksGame.Objects.ReplyObjects
{
	public class Bullet
	{
		/// <summary>
		/// Location of Bullet
		/// </summary>
		public Point Location { get; set; }

		/// <summary>
		/// Bullet Dirn [0 : 3]
		/// </summary>
		public Directions Direction { get; set; }

		/// <summary>
		/// Is the bullet alive
		/// </summary>
		public bool IsLive { get; set; }

		public Bullet()
		{
			IsLive = false;
		}
	}
}
