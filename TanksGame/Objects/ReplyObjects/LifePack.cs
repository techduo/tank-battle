﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Drawing;
using Microsoft.Xna.Framework;

namespace TanksGame.Objects.ReplyObjects
{
    public class LifePack:Data
    {
        /// <summary>
        /// Location of life pack
        /// </summary>
        public Point Location { get; set; }

        /// <summary>
        /// Life time of life pack
        /// </summary>
        public long LifeTime { get; set; }
    }
}
