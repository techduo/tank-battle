﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Drawing;
using Microsoft.Xna.Framework;

namespace TanksGame.Objects.ReplyObjects
{
	public class MapObstacle : Data
	{
		/// <summary>
		/// Player ID [1 : 5]
		/// </summary>
		public int ClientPlayerID { get; set; }

		/// <summary>
		/// Brick wall obstacle coordinate list
		/// </summary>
		public List<Point> BrickWalls { get; set; }

		/// <summary>
		/// Stone wall obstacle coordinate list
		/// </summary>
		public List<Point> StoneWalls { get; set; }

		/// <summary>
		/// water obstacle coordinate list
		/// </summary>
		public List<Point> WaterBlocks { get; set; }
	}
}
