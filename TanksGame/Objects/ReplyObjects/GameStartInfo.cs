﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TanksGame.Objects.ReplyObjects
{
	public class GameStartInfo : Data
	{
		/// <summary>
		/// Intital player Information [1-5]
		/// </summary>
		public List<Player> Players { get; set; }
	}
}
