﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TanksGame.Objects;

namespace TanksGame.Objects.ReplyObjects
{
	public class Obstacle : Data
	{
		public int Lost_Points { get; set; }
	}
}
