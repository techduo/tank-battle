﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TanksGame.Objects.ReplyObjects
{
	public class GameVariableUpdate : Data
	{

		/// <summary>
		/// Player Information [1-5]
		/// </summary>
		public List<Player> Players { get; set; }

		/// <summary>
		///Brick wall damage list [7]
		/// </summary>
		public List<BrickWallDamageReport> BrickWallDamage { get; set; }
	}
}
