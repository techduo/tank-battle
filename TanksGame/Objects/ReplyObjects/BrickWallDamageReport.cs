﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Drawing;
using Microsoft.Xna.Framework;

namespace TanksGame.Objects.ReplyObjects
{
	public class BrickWallDamageReport
	{

		/// <summary>
		/// Wall Location [(0, 0) : (20, 20)]
		/// </summary>
		public Point Location { get; set; }

		/// <summary>
		/// Wall Damge [0-4]
		/// </summary>
		public int Damage { get; set; }
	}
}
