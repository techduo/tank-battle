﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Drawing;
using Microsoft.Xna.Framework;

namespace TanksGame.Objects.ReplyObjects
{
    public class CoinPile : Data
    {
        /// <summary>
        /// Location of coin pile
        /// </summary>
        public Point Location { get; set; }

        /// <summary>
        /// Life time of coin pile
        /// </summary>
        public long LifeTime { get; set; }

        /// <summary>
        /// Value of the coin pile
        /// </summary>
        public long Value { get; set; }
    }
}
