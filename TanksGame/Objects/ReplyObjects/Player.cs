﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
//using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TanksGame.Objects.ReplyObjects
{
	public class Player 
	{
		/// <summary>
		/// Player ID [1 : 5]
		/// </summary>
		public int PlayerID { get; set; }

		/// <summary>
		/// Player Location [(0, 0) : (20, 20)]
		/// </summary>
		public Point Location { get; set; }

		/// <summary>
		/// Player Dirn [0 : 3]
		/// </summary>
		public Directions Direction { get; set; }

        /// <summary>
        /// Defines display Location
        /// </summary>
        public Point Position { get; set; }

        /// <summary>
        /// Defines display rotation
        /// </summary>
        public double Rotation { get; set; }

		/// <summary>
		///whether Player shot a shell [true : false]
		/// </summary>
		public int Shot { get; set; }

		/// <summary>
		/// Player health
		/// </summary>
		public int Health { get; set; }

		/// <summary>
		/// Player coins
		/// </summary>
		public long Coins { get; set; }

		/// <summary>
		/// Player points
		/// </summary>
		public long Points { get; set; }


        public static double DirectionToRadians(Directions direction)
        {
            if (direction == Directions.North)
            {
                return 0;
            } else if (direction == Directions.East)
            {
                return Math.PI / 2;
            } else if (direction == Directions.South)
            {
                return Math.PI;
            } else
            {
                return 3 * Math.PI / 2;
            }
        }
	}

	public enum Directions
	{
		North = 0,
		East = 1,
		South = 2,
		West = 3
	}
}
