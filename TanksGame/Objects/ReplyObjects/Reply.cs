﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TanksGame.Objects.ReplyObjects
{
	/// <summary>
	/// Container for Message only Communication with the server
	/// </summary>
	public class Reply : GObject
	{
		/// <summary>
		/// Message Type
		/// </summary>
		public ReplyTypes MessageType { get; set; }

		/// <summary>
		/// Embedded Data
		/// if (NoData) return Null;
		/// if (MapInstance) return Map;
		/// if (Player) return Player;...
		/// </summary>
		public Data Data { get; set; }

		public Reply()
		{
			MessageType = ReplyTypes.NOT_SET;
		}
	}

	public enum ReplyTypes
	{
		NOT_SET = -1,

		PLAYERS_FULL = 0,
		ALREADY_ADDED = 1,
		GAME_ALREADY_STARTED = 2,
		GAME_START = 3,

		//
		PLAYER_DATA = 4,
		MAP_DATA = 5,

		//
		OBSTACLE = 6,
		CELL_OCCUPIED = 7,
		DEAD = 8,
		TOO_QUICK = 9,
		INVALID_CELL = 10,
		GAME_HAS_FINISHED = 11,
		GAME_NOT_STARTED_YET = 12,
		NOT_A_VALID_CONTESTANT = 13,

		//
		GLOBAL_DATA_UPDATE = 14,

		//
		COIN_DATA = 15,
		LIFE_PACK_DATA = 16,

		//
		GAME_FINISHED = 17,

		//Found on server log
		PITFALL = 18
	}
}
