﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System;
using System.Linq;

namespace TanksGame.Controls
{
    #region Deligades
    /// <summary>
    /// Keys Event Handler
    /// </summary>
    /// <param name="tool">Tool the event is created</param>
    /// <param name="e">Event details</param>
    public delegate void KeysEventHandler(Control tool, KeysEventArgs e);
    #endregion

    #region ControlTool
    public abstract class Control : DrawableGameComponent
    {
        #region Attributes 
        protected SpriteBatch spriteBatch;
        // private Game game;

        private string text;
        private bool isFocused;

        // Keyboard Management
        protected KeyboardState keyboardState;
        protected KeyboardState oldKeyboardState;
        #endregion

        #region Properties
        /// <summary>
        /// Back Color
        /// </summary>
        public Color BackColor { get; set; }

        /// <summary>
        /// Border style
        /// </summary>
        public BorderStyle BorderStyle { get; set; }

        /// <summary>
        /// Dispayed Text
        /// </summary>
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                if (TextChanged != null) TextChanged(this, new EventArgs());
            }
        }

        // public bool Enabled;

        /// <summary>
        /// Text Font
        /// </summary>
        public SpriteFont Font { get; set; }

        /// <summary>
        /// Color of fonts (foreground)
        /// </summary>
        public Color ForeColor { get; set; }

        /// <summary>
        /// Size - Width, Height 
        /// </summary>
        public Vector2 Size { get; set; }

        /// <summary>
        /// Position - Top, Left
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        /// Name of the conrtol for identification
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Background Image
        /// </summary>
        public Texture2D Image { get; set; }

        /// <summary>
        /// When to give the Control access
        /// </summary>
        public int TabIndex { get; set; }

        /// <summary>
        /// Is a Tab Stop
        /// </summary>
        public bool TabStop { get; set; }

        /// <summary>
        /// Parent panel (Control inherits some properties of Parent panel)
        /// </summary>
        public Panel Parent { get; set; }


        /// <summary>
        /// Is control Focused - Should it listen to key changes
        /// </summary>
        public bool IsFocused
        {
            get
            {
                return isFocused;
            }
            set
            {
                isFocused = value;
                if (value == true)
                    if (GotFocus != null) GotFocus(this, new EventArgs());
                    else
                    if (LostFocus != null) LostFocus(this, new EventArgs());
            }
        }
        #endregion

        #region EventHandelers
        /// <summary>
        /// Invoked when text is changed
        /// </summary>
        public event EventHandler TextChanged;

        /// <summary>
        /// Invoked when key is down while the control is focued
        /// </summary>
        public event KeysEventHandler KeyDown;

        /// <summary>
        /// Invoked when key is pressed while the control is focued
        /// </summary>
        public event KeysEventHandler KeyPress;

        /// <summary>
        /// Invoked when key is up while the control is focued
        /// </summary>
        public event KeysEventHandler KeyUp;

        /// <summary>
        /// Invoked when control is focused
        /// </summary>
        public event EventHandler GotFocus;

        /// <summary>
        /// Invoked when control has lost focus
        /// </summary>
        public event EventHandler LostFocus;
        #endregion

        #region Methods
        public Control(Game game, SpriteBatch spriteBatch) : base(game)
        {
            this.Text = "";
            this.spriteBatch = spriteBatch;
            this.BackColor = Color.Black;
            this.ForeColor = Color.White;
            this.Font = game.Content.Load<SpriteFont>("fonts/controlFont");
        }

        /// <summary>
        /// Set focus to the Control
        /// </summary>
        public void Focus()
        {
            IsFocused = true;
        }

        /// <summary>
        /// Check for key Press
        /// </summary>
        /// <param name="aKey">The key to be checked</param>
        /// <returns></returns>
        private Keys[] GetPressedKeys()
        {
            return oldKeyboardState.GetPressedKeys().Except<Keys>(keyboardState.GetPressedKeys()).ToArray();
        }

        /// <summary>
        /// Initialize base
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Update (XNA)
        /// </summary>
        /// <param name="gameTime">Game Time</param>
        public override void Update(GameTime gameTime)
        {
            if (IsFocused)
            {
                keyboardState = Keyboard.GetState();
                // TODO: Invoke necessary Events
                // Get all clisked(Pressed) keys
                Keys[] keys = GetPressedKeys();
                // Notify all key UP
                if (keys.Length > 0)
                {
                    KeysEventArgs e = new KeysEventArgs(keys);
                    if (KeyUp != null) KeyUp(this, e);
                }
                // Get currently pressed(down) keys
                Keys[] keysPressed = keyboardState.GetPressedKeys();
                // Notify all key DOWN
                if (keysPressed.Length > 0)
                {
                    KeysEventArgs e = new KeysEventArgs(keysPressed);
                    if (KeyDown != null) KeyDown(this, e);
                }
                // TODO: Update Control 
                UpdateControl(keys, keysPressed, gameTime);
                // END TODO
                // Notify Key Press
                if (keys.Length > 0)
                {
                    KeysEventArgs e = new KeysEventArgs(keys);
                    if (KeyPress != null) KeyPress(this, e);
                }
                // END TODO
                base.Update(gameTime);
                oldKeyboardState = keyboardState;
            }
        }

        /// <summary>
        /// Game Draw (XNA)
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            // TODO: Draw Control 
            DrawControl(gameTime);
            // END TODO
        }

        /// <summary>
        /// Update Control
        /// </summary>
        /// <param name="keys">Keys Pressed</param>
        /// <param name="KeysDown">Keys Down</param>
        public abstract void UpdateControl(Keys[] keys, Keys[] KeysDown, GameTime gameTime);

        /// <summary>
        /// Draw Control
        /// </summary>
        public abstract void DrawControl(GameTime gameTime);
        #endregion
    }
    #endregion

    #region Support
    public enum BorderStyle
    {
        none,
        flat,
        _3D
    }

    public class KeysEventArgs : EventArgs
    {
        public Keys[] keys { get; private set; }

        public KeysEventArgs(Keys[] keys) : base()
        {
            this.keys = keys;
        }
    }

    #endregion
}
