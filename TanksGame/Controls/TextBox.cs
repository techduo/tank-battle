﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace TanksGame.Controls
{
    class TextBox : Control
    {
        #region Attributes
        Texture2D cursor;
        private Vector2 cursorPosition;

        TimeSpan blinkTime;
        bool blink;

        Keys[] keysToCheck = new Keys[] {
             Keys.A, Keys.B, Keys.C, Keys.D, Keys.E,
             Keys.F, Keys.G, Keys.H, Keys.I, Keys.J,
             Keys.K, Keys.L, Keys.M, Keys.N, Keys.O,
             Keys.P, Keys.Q, Keys.R, Keys.S, Keys.T,
             Keys.U, Keys.V, Keys.W, Keys.X, Keys.Y,
             Keys.NumPad0, Keys.NumPad1, Keys.NumPad2, Keys.NumPad3, Keys.NumPad4,
             Keys.NumPad5, Keys.NumPad6, Keys.NumPad7, Keys.NumPad8, Keys.NumPad9,
             Keys.D0, Keys.D1, Keys.D2, Keys.D3, Keys.D4,
             Keys.D5, Keys.D6, Keys.D7, Keys.D8, Keys.D9,
             Keys.OemPeriod, Keys.Decimal,
             Keys.Z, Keys.Back, Keys.Space };
        #endregion

        #region Methods
        public TextBox(Game game, SpriteBatch spriteBatch) : base(game, spriteBatch)
        {
            // Set the position
            // Center of Screen: position = new Vector2((Game.Window.ClientBounds.Width - width) / 2, (Game.Window.ClientBounds.Height - height) / 2);
            // Left Down Corner: 
            this.cursor = game.Content.Load<Texture2D>("components/cursor");
            this.Image = game.Content.Load<Texture2D>("components/textbox");
            this.Size = new Vector2(120, 35);
            this.cursorPosition = new Vector2(0, 0);
            this.ForeColor = Color.Black;
        }

        public override void UpdateControl(Keys[] keys, Keys[] KeysDown, GameTime gameTime)
        {
            blinkTime += gameTime.ElapsedGameTime;
            if (blinkTime > TimeSpan.FromMilliseconds(500))
            {
                blink = !blink;
                blinkTime -= TimeSpan.FromMilliseconds(500);
            }
            foreach (Keys key in keysToCheck)
            {
                if (CheckKey(key))
                {
                    AddKeyToText(key);
                    break;
                }
            }
            Vector2 textSize = Font.MeasureString(Text);
            cursorPosition.X = textSize.X;
        }

        public override void DrawControl(GameTime gameTime)
        {
            Vector2 location = Position;
            if (Parent != null)
            {
                location += Parent.Position;
            }

            spriteBatch.Draw(Image, new Rectangle(location.ToPoint(), Size.ToPoint()), Color.White);

            if (!blink && IsFocused)
                spriteBatch.Draw(cursor, 
                    new Rectangle((location + cursorPosition).ToPoint() + new Point(4,  5), 
                    new Point((int)Size.Y / 18, (int)Size.Y - 12)), Color.White);

            spriteBatch.DrawString(Font, Text, location + new Vector2(3, 3), ForeColor);
            //base.Draw(gameTime);
        }

        /// <summary>
        /// Add Key to text
        /// </summary>
        /// <param name="key"></param>
        private void AddKeyToText(Keys key)
        {
            string newChar = "";
            if (Text.Length >= 16 && key != Keys.Back)
                return;
            switch (key)
            {
                case Keys.A:
                    newChar += "a";
                    break;
                case Keys.B:
                    newChar += "b";
                    break;
                case Keys.C:
                    newChar += "c";
                    break;
                case Keys.D:
                    newChar += "d";
                    break;
                case Keys.E:
                    newChar += "e";
                    break;
                case Keys.F:
                    newChar += "f";
                    break;
                case Keys.G:
                    newChar += "g";
                    break;
                case Keys.H:
                    newChar += "h";
                    break;
                case Keys.I:
                    newChar += "i";
                    break;
                case Keys.J:
                    newChar += "j";
                    break;
                case Keys.K:
                    newChar += "k";
                    break;
                case Keys.L:
                    newChar += "l";
                    break;
                case Keys.M:
                    newChar += "m";
                    break;
                case Keys.N:
                    newChar += "n";
                    break;
                case Keys.O:
                    newChar += "o";
                    break;
                case Keys.P:
                    newChar += "p";
                    break;
                case Keys.Q:
                    newChar += "q";
                    break;
                case Keys.R:
                    newChar += "r";
                    break;
                case Keys.S:
                    newChar += "s";
                    break;
                case Keys.T:
                    newChar += "t";
                    break;
                case Keys.U:
                    newChar += "u";
                    break;
                case Keys.V:
                    newChar += "v";
                    break;
                case Keys.W:
                    newChar += "w";
                    break;
                case Keys.X:
                    newChar += "x";
                    break;
                case Keys.Y:
                    newChar += "y";
                    break;
                case Keys.Z:
                    newChar += "z";
                    break;
                case Keys.D0:
                case Keys.NumPad0:
                    newChar += "0";
                    break;
                case Keys.D1:
                case Keys.NumPad1:
                    newChar += "1";
                    break;
                case Keys.D2:
                case Keys.NumPad2:
                    newChar += "2";
                    break;
                case Keys.D3:
                case Keys.NumPad3:
                    newChar += "3";
                    break;
                case Keys.D4:
                case Keys.NumPad4:
                    newChar += "4";
                    break;
                case Keys.D5:
                case Keys.NumPad5:
                    newChar += "5";
                    break;
                case Keys.D6:
                case Keys.NumPad6:
                    newChar += "6";
                    break;
                case Keys.D7:
                case Keys.NumPad7:
                    newChar += "7";
                    break;
                case Keys.D8:
                case Keys.NumPad8:
                    newChar += "8";
                    break;
                case Keys.D9:
                case Keys.NumPad9:
                    newChar += "9";
                    break;
                case Keys.Space:
                    newChar += " ";
                    break;
                case Keys.OemPeriod:
                case Keys.Decimal:
                    newChar += ".";
                    break;
                case Keys.Back:
                    if (Text.Length != 0)
                        Text = Text.Remove(Text.Length - 1);
                    return;
            }
            if (keyboardState.IsKeyDown(Keys.RightShift) ||
            keyboardState.IsKeyDown(Keys.LeftShift))
            {
                newChar = newChar.ToUpper();
            }
            Text += newChar;
        }

        /// <summary>
        /// Checks the key for key press
        /// </summary>
        /// <param name="aKey"></param>
        /// <returns></returns>
        private bool CheckKey(Keys aKey)
        {
            return oldKeyboardState.IsKeyDown(aKey) &&
           keyboardState.IsKeyUp(aKey);
        }
        #endregion
    }
}
