﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace TanksGame.Controls
{
    class Label : Control
    {
        #region Attributes
        #endregion

        #region Methods
        public Label(Game game, SpriteBatch spriteBatch) : base(game, spriteBatch)
        {
            // Set the position
            // Center of Screen: position = new Vector2((Game.Window.ClientBounds.Width - width) / 2, (Game.Window.ClientBounds.Height - height) / 2);
            // Left Down Corner: 
            // this.Image = game.Content.Load<Texture2D>("components/label");
            this.Size = new Vector2(100, 23);
            this.BackColor = Color.Transparent;
        }

        public override void UpdateControl(Keys[] keys, Keys[] KeysDown, GameTime gameTime)
        {
            // No action
        }

        public override void DrawControl(GameTime gameTime)
        {
            Vector2 location = Position;
            if (Parent != null)
            {
                location += Parent.Position;
            }
            if (Image != null)
            {
                spriteBatch.Draw(Image, new Rectangle(location.ToPoint(), Size.ToPoint()), Color.White);
            }
            spriteBatch.DrawString(Font, Text, location + new Vector2(3, -1), ForeColor);
            //base.Draw(gameTime);
        }
        #endregion
    }
}
