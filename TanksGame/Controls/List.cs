﻿using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;


namespace TanksGame.Controls
{
    class List : Control
    {
        #region Attributes
        // Menu items
        private string[] menuItems;
        private int selectedIndex;
        #endregion

        #region Properties
        /// <summary>
        /// Set/ Get Hilite Color
        /// </summary>
        public Color HiliteColor { get; set; }

        /// <summary>
        /// Selected index Managed Set and Get
        /// Zero based
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
            set
            {
                selectedIndex = value;
                if (selectedIndex < 0)
                    selectedIndex = 0;
                if (selectedIndex >= menuItems.Length)
                    selectedIndex = menuItems.Length - 1;
            }
        }
        #endregion

        #region Methods
        public List(Game game, SpriteBatch spriteBatch, string[] menuItems) : base(game, spriteBatch)
        {
            this.menuItems = menuItems;
            MeasureMenu();
            this.HiliteColor = Color.Orange;
            // Set the position
            // Center of Screen: position = new Vector2((Game.Window.ClientBounds.Width - width) / 2, (Game.Window.ClientBounds.Height - height) / 2);
            // Left Down Corner: 
        }

        /// <summary>
        /// Set height, idth and location(to center of screen) of menu
        /// </summary>
        private void MeasureMenu()
        {
            float height = 0;
            float width = 0;
            foreach (string item in menuItems)
            {
                Vector2 size = Font.MeasureString(item);
                if (size.X > width)
                    width = size.X;
                height += Font.LineSpacing + 15;
            }

            Size = new Vector2(width, height);
            this.Position = new Vector2(5, Game.Window.ClientBounds.Height - Size.Y);
        }

        /// <summary>
        /// Update Control
        /// </summary>
        /// <param name="keys">Keys Pressed</param>
        /// <param name="KeysDown">Keys Down</param>
        public override void UpdateControl(Keys[] keys, Keys[] keysPressed, GameTime gameTime)
        {
            if (keys.Contains(Keys.Down))
            {
                selectedIndex++;
                if (selectedIndex == menuItems.Length)
                    selectedIndex = 0;
            }
            if (keys.Contains(Keys.Up))
            {
                selectedIndex--;
                if (selectedIndex < 0)
                    selectedIndex = menuItems.Length - 1;
            }
        }

        /// <summary>
        /// Draw Control
        /// </summary>
        public override void DrawControl(GameTime gameTime)
        {
            Vector2 location = Position;
            if (Parent != null)
            {
                location += Parent.Position;
            }

            Color tint;

            string menuItem;
            for (int i = 0; i < menuItems.Length; i++)
            {
                if (i == selectedIndex)
                {
                    tint = HiliteColor;
                    menuItem = " " + menuItems[i];
                }
                else
                {
                    tint = ForeColor;
                    menuItem = menuItems[i];
                }

                spriteBatch.DrawString(Font, menuItem, location, tint);
                location.Y += Font.LineSpacing + 5;
            }
        }
        #endregion
    }
}
