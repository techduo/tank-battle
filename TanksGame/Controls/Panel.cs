﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace TanksGame.Controls
{
    public class Panel : Control
    {
        // List of Game Components
        List<GameComponent> components = new List<GameComponent>();

        public Panel(Game game, SpriteBatch spriteBatch) : base(game, spriteBatch)
        {
            this.Size = new Vector2(100, 250);
            this.BackColor = Color.Transparent;
        }


        public override void UpdateControl(Keys[] keys, Keys[] KeysDown, GameTime gameTime)
        {
            foreach (GameComponent component in components)
                if (component.Enabled == true)
                    component.Update(gameTime);
        }

        public override void DrawControl(GameTime gameTime)
        {
            foreach (GameComponent component in components)
                if (component is DrawableGameComponent &&
                ((DrawableGameComponent)component).Visible)
                    ((DrawableGameComponent)component).Draw(gameTime);
        }

        public void Add(Control control) {
            components.Add(control);
            control.Parent = this;
        }

        /// <summary>
        /// Set visibility of the game component to be true
        /// </summary>
        public virtual void Show()
        {
            this.Visible = true;
            this.Enabled = true;
            foreach (GameComponent component in components)
            {
                component.Enabled = true;
                if (component is DrawableGameComponent)
                    ((DrawableGameComponent)component).Visible = true;
            }
        }

        /// <summary>
        /// Set visibility of the game component to be false
        /// </summary>
        public virtual void Hide()
        {
            this.Visible = false;
            this.Enabled = false;
            foreach (GameComponent component in components)
            {
                component.Enabled = false;
                if (component is DrawableGameComponent)
                    ((DrawableGameComponent)component).Visible = false;
            }
        }
    }
}
