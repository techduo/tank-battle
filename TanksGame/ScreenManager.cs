﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TanksGame.Screens;

namespace TanksGame
{
    public partial class ScreenManager
    {
        // Active Screen
        Screen activeScreen;
        // All screens
        MenuScreen menuScreen;
        GameScreen gameScreen;
        SettingsScreen settingsScreen;
        // Keyboard Management
        private KeyboardState keyboardState;
        private KeyboardState oldKeyboardState;

        /// <summary>
        /// Initializes Game
        /// </summary>
        partial void InitializeGame()
        {
            // TODO: 
        }

        /// <summary>
        /// Load Components Here
        /// </summary>
        partial void LoadComponents()
        {
            // TODO:
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // Create menu Screen
            menuScreen = new MenuScreen(
                        this,
                        spriteBatch,
                        Content.Load<SpriteFont>("menufont"),
                        Content.Load<Texture2D>("tanksWallpaper"));
            Components.Add(menuScreen);
            menuScreen.Hide();

            gameScreen = new GameScreen(
                        this,
                        spriteBatch);
            Components.Add(gameScreen);
            gameScreen.Hide();

            settingsScreen = new SettingsScreen(
                        this,
                        spriteBatch,
                        Content.Load<Texture2D>("tanksWallpaper"),
                        Content.Load<SpriteFont>("menufont"));
            Components.Add(settingsScreen);
            settingsScreen.Hide();

            activeScreen = menuScreen;
            activeScreen.Show();
        }

        /// <summary>
        /// Unload Components Here
        /// </summary>
        partial void UnloadComponents()
        {
            // TODO:
        }

        /// <summary>
        /// Draw Game Components in order
        /// </summary>
        /// <param name="gameTime">Game system time</param>
        partial void DrawComponents(GameTime gameTime)
        {
            // TODO: 
            spriteBatch.Begin();
            base.Draw(gameTime);
            spriteBatch.End();
        }

        /// <summary>
        /// Update Game Components in order
        /// </summary>
        /// <param name="gameTime">Game system time</param>
        partial void UpdateComponents(GameTime gameTime)
        {
            // TODO: 
            keyboardState = Keyboard.GetState();
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            if (activeScreen == menuScreen)
            {
                if (CheckKey(Keys.Enter))
                {
                    if (menuScreen.SelectedIndex == 0)
                    {
                        activeScreen.Hide();
                        activeScreen = gameScreen;
                        activeScreen.Show();
                        if (gameScreen.GameOnPlay()) gameScreen.UnLoadGame();
                        gameScreen.LoadGame();
                    }
                    if (menuScreen.SelectedIndex == 1)
                    {
                        activeScreen.Hide();
                        activeScreen = gameScreen;
                        activeScreen.Show();
                        if (!gameScreen.GameOnPlay())
                            gameScreen.LoadGame();
                    }
                    if (menuScreen.SelectedIndex == 2)
                    {
                        activeScreen.Hide();
                        activeScreen = settingsScreen;
                        activeScreen.Show();
                    }
                    if (menuScreen.SelectedIndex == 3)
                    {
                        System.Diagnostics.Process.Start("http://ysenarath.github.io/TechTanks/");
                    }
                    if (menuScreen.SelectedIndex == 4)
                    {
                        if (gameScreen.GameOnPlay()) gameScreen.UnLoadGame();
                        this.Exit();
                    }
                }
            }
            else if (activeScreen == gameScreen)
            {
                if (CheckKey(Keys.Back)) {
                    activeScreen.Hide();
                    activeScreen = menuScreen;
                    activeScreen.Show();
                }
            }
            base.Update(gameTime);
            oldKeyboardState = keyboardState;
        }

        /// <summary>
        /// Check for key Press
        /// </summary>
        /// <param name="aKey">The key to be checked</param>
        /// <returns></returns>
        private bool CheckKey(Keys aKey)
        {
            return keyboardState.IsKeyUp(aKey) && oldKeyboardState.IsKeyDown(aKey);
        }

        public void ActivateMainScreen() {
            activeScreen.Hide();
            activeScreen = menuScreen;
            activeScreen.Show();
        }
    }
}
