﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TanksGame.Engine;

namespace TanksGame.AI
{
	/// <summary>
	/// Abstract class to implement the Ai of the game
	/// </summary>
    public abstract class GameAi
    {
		/// <summary>
		/// point to game objects
		/// </summary>
        protected GameObjectHolder gameHolder;

        public GameAi(GameObjectHolder gameHolder) {
            this.gameHolder = gameHolder;
        }

        /// <summary>
        /// Get the next move locaion
        /// </summary>
        /// <returns></returns>
        public abstract string getNextCommand();
    }
}
