﻿using Microsoft.Xna.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TanksGame.Engine;
using TanksGame.Objects.ReplyObjects;

namespace TanksGame.AI
{
    /// <summary>
    /// Simple Dumb Ai
    /// :(
    /// </summary>
    class ConcreteAI : GameAi
    {

        private int minHealth;
        private int maxHealth;
        private bool hasMinHealthReached;

        private enum MatchAxis
        {
            X = 0,
            Y = 1
        }

        /// <summary>
        /// Concrete AI constructor
        /// </summary>
        /// <param name="holder"></param>
        public ConcreteAI(GameObjectHolder holder) : base(holder)
        {
            hasMinHealthReached = false;
            minHealth = 60;
            maxHealth = 100;
        }

        /// <summary>
        /// Always move Right
        /// Dumb AI
        /// </summary>
        /// <returns></returns>
        public override string getNextCommand()
        {
            // Defence Action
            string defence = GetDefenceAction();
            if (defence != "")
            {
                return defence;
            }

            //Try shooting near by enemies first
            string offensiveAction = GetOffenciveActionEnemy();
            if (offensiveAction != "")
            {
                return offensiveAction;
            }

            // Try to collect all coins/healthpacks
            string coinshpExistAction = getNextMoveNormalMode();
            if (CoinsExist() && coinshpExistAction != "")
            {
                return coinshpExistAction;
            }

            return GetOffensiveAction();
        }

        #region Defence action

        /// <summary>
        /// Get defence action
        /// </summary>
        /// <returns>Returns game command; empty if no action is needed/ unable to avoid</returns>
        private string GetDefenceAction()
        {
            if (gameHolder.PlayerId >= 0 && gameHolder.Players[gameHolder.PlayerId] != null)
            {
                Player me = gameHolder.Players[gameHolder.PlayerId];
                String command = "";
                if (me.Health <= minHealth)
                {
                    hasMinHealthReached = true;
                    foreach (Player p in gameHolder.Players)
                    {
                        if (p != null && p != me)
                        {
                            if (p.Shot == 1)
                            {
                                Point dirn = p.Location - me.Location;
                                if (dirn.X == 0 && (dirn.Y > 0 && p.Direction == Directions.North ||
                                    dirn.Y < 0 && p.Direction == Directions.South))
                                {
                                    if (CheckForNoObstacles(MatchAxis.Y, p.Location.Y, me.Location.X, p.Location.X, true))
                                    {
                                        command = EvadeBulletFrom(MatchAxis.Y, me.Location);
                                    }
                                }
                                else if (dirn.Y == 0 && (dirn.X > 0 && p.Direction == Directions.West ||
                                    dirn.X < 0 && p.Direction == Directions.East))
                                {
                                    if (CheckForNoObstacles(MatchAxis.X, p.Location.X, me.Location.Y, p.Location.Y, true))
                                    {
                                        command = EvadeBulletFrom(MatchAxis.X, me.Location);
                                    }
                                }
                            }
                        }
                    }
                }
                else if (me.Health >= maxHealth)
                {
                    hasMinHealthReached = false;
                }
                return command;
            }
            return "";
        }


        /// <summary>
        /// Get command to evade bullet from a given direction
        /// </summary>
        /// <param name="axis">Axix needed to be avoided</param>
        /// <param name="myLoc">My current position</param>
        /// <returns></returns>
        private string EvadeBulletFrom(MatchAxis axis, Point myLoc)
        {
            if (axis == MatchAxis.X)
            {
                if (canMove(myLoc + new Point(0, -1)) && !HasPlayer(myLoc + new Point(0, -1)))
                {
                    return GameCommands.MoveUp;
                }
                else if (canMove(myLoc + new Point(0, 1)) && !HasPlayer(myLoc + new Point(0, 1)))
                {
                    return GameCommands.MoveDown;
                }
            }
            else
            {
                if (canMove(myLoc + new Point(-1, 0)) && !HasPlayer(myLoc + new Point(-1, 0)))
                {
                    return GameCommands.MoveLeft;
                }
                else if (canMove(myLoc + new Point(1, 0)) && !HasPlayer(myLoc + new Point(1, 0)))
                {
                    return GameCommands.MoveRight;
                }
            }
            return "";
        }
        #endregion

        #region CoinPile present action

        private bool CoinsExist()
        {
            long[] b = (from cp in gameHolder.CoinPiles
                        where cp != null && cp.LifeTime > 0
                        select cp.LifeTime).ToArray();
            return b.Length > 0;
        }

        private string getNextMoveNormalMode()
        {
            if (gameHolder.StateOfGame == ReplyTypes.GAME_START)
            {
                Player me = gameHolder.Players[gameHolder.PlayerId];
                Point currentPt = me.Location;

                Point p = moveDirection(currentPt);

                if (p.X == 1)
                {
                    return GameCommands.MoveRight;
                }
                else if (p.X == -1)
                {
                    return GameCommands.MoveLeft;
                }
                else if (p.Y == 1)
                {
                    return GameCommands.MoveDown;
                }
                else if (p.Y == -1)
                {
                    return GameCommands.MoveUp;
                }
                return "";
            }
            else
                return "";
        }

        /// <summary>
        /// Get the next possible move.
        /// </summary>
        /// <param name="source">Source point of Tank</param>
        /// <returns>Direction of motion</returns>
        public Point moveDirection(Point source)
        {
            // List of movable points
            List<Point> q = new List<Point>();
            int[,] grid = new int[GameSettings.GridSize, GameSettings.GridSize];
            int[,] dist = new int[GameSettings.GridSize, GameSettings.GridSize];
            Point[,] prev = new Point[GameSettings.GridSize, GameSettings.GridSize];
            for (int i = 0; i < GameSettings.GridSize; i++)
            {
                for (int j = 0; j < GameSettings.GridSize; j++)
                {
                    Point pt = new Point(i, j);
                    prev[i, j] = new Point(-1, -1);
                    dist[i, j] = int.MaxValue;
                    if (canMove(pt) && isSafe(pt))
                    {
                        grid[i, j] = 0;
                        q.Add(pt);
                    }
                    else
                    {
                        grid[i, j] = -1;
                    }
                }
            }
            // q.Add(new Point(source.X, source.Y));

            dist[source.X, source.Y] = 0;
            // q.Add(new Point(0, 0));

            Point destination = new Point(0, 0);
            long maxCollectablePoints = 0;

            // Select best alternative
            while (q != null && q.Count > 0)
            {
                Point u = getLeastOf(q, dist);

                q.Remove(u);

                int alt = dist[u.X, u.Y] + 1;

                Point x1 = u + new Point(1, 0); // X+
                Point x2 = u + new Point(-1, 0); // X -
                Point y1 = u + new Point(0, 1); // y+
                Point y2 = u + new Point(0, -1); // y-

                Point v = x1;

                if (v.X < GameSettings.GridSize && grid[v.X, v.Y] != -1)
                {
                    if (alt < dist[v.X, v.Y])
                    {
                        dist[v.X, v.Y] = alt;
                        prev[v.X, v.Y] = u;
                    }
                }

                v = x2;
                if (v.X >= 0 && grid[v.X, v.Y] != -1)
                {
                    if (alt < dist[v.X, v.Y])
                    {
                        dist[v.X, v.Y] = alt;
                        prev[v.X, v.Y] = u;
                    }
                }

                v = y1;
                if (v.Y < GameSettings.GridSize && grid[v.X, v.Y] != -1)
                {
                    if (alt < dist[v.X, v.Y])
                    {
                        dist[v.X, v.Y] = alt;
                        prev[v.X, v.Y] = u;
                    }
                }

                v = y2;
                if (v.Y >= 0 && grid[v.X, v.Y] != -1)
                {
                    if (alt < dist[v.X, v.Y])
                    {
                        dist[v.X, v.Y] = alt;
                        prev[v.X, v.Y] = u;
                    }
                }

                long cp = CollectablePoints(u);

                if (cp > maxCollectablePoints)
                {
                    destination = beginingPoint(prev, u, source);
                    maxCollectablePoints = cp;
                }
            }
            // If there is no player then move to given location
            if (!HasPlayer(destination))
            {
                return destination - source;
            }
            return new Point(0, 0);
        }

        /// <summary>
        /// Whether given point is safe or not
        /// </summary>
        /// <param name="pt"></param>
        /// <returns></returns>
        private bool isSafe(Point pt)
        {
            if (gameHolder.PlayerId >= 0 && gameHolder.Players[gameHolder.PlayerId] != null && gameHolder.Players[gameHolder.PlayerId].Health <= minHealth)
            {
                Player me = gameHolder.Players[gameHolder.PlayerId];
                foreach (Player p in gameHolder.Players)
                {
                    if (p != null && p.Health > 0 && p != me)
                    {
                        Point dirn = p.Location - me.Location;
                        if (dirn.X == 0 && (dirn.Y > 0 &&
                            p.Direction == Directions.North || dirn.Y < 0 && p.Direction == Directions.South) &&
                            CheckForNoObstacles(MatchAxis.Y, p.Location.Y, me.Location.X, p.Location.X, true))
                        {
                            return false;
                        }
                        else if (dirn.Y == 0 && 
                            (dirn.X > 0 && p.Direction == Directions.West || dirn.X < 0 && p.Direction == Directions.East) &&
                            CheckForNoObstacles(MatchAxis.X, p.Location.X, me.Location.Y, p.Location.Y, true))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Backtrack the source from destination
        /// </summary>
        /// <param name="prev">Previous list(table)</param>
        /// <param name="child">Find from</param>
        /// <param name="source">Source</param>
        /// <returns>Next cell for completing given path</returns>
        public Point beginingPoint(Point[,] prev, Point child, Point source)
        {
            Point old = new Point(0, 0);
            Point p = child;
            while (source.X != p.X || source.Y != p.Y)
            {
                old = p;
                if (p.X == -1 || p.Y == -1)
                    return source;
                p = prev[p.X, p.Y];
            }
            return old;
        }

        /// <summary>
        /// Get the least value referaing to a position
        /// </summary>
        /// <param name="q">List of possible moves</param>
        /// <param name="dist">distance from the source(Calculated)</param>
        /// <returns>Position of the least value</returns>
        private Point getLeastOf(List<Point> q, int[,] dist)
        {
            if (q.Count == 0) throw new ArgumentNullException("Can't obtain a least distance of empty list.");
            int i = 0;
            int min = dist[q[i].X, q[i].Y];
            Point minVertex = q[i];
            for (i = 1; i < q.Count; i++)
            {
                int val = dist[q[i].X, q[i].Y];
                if (val < min)
                {
                    min = val;
                    minVertex = q[i];
                }
            }
            return minVertex;
        }

        /// <summary>
        /// Get whether a coinpile is at given position
        /// </summary>
        /// <param name="pt">Location to be examined</param>
        /// <returns>No of points for getting point</returns>
        private long CollectablePoints(Point pt)
        {
            if (hasMinHealthReached)
            {
                long[] l = (from p in gameHolder.LifePacks
                            where p != null && p.Location == pt && p.LifeTime > 0
                            select p.LifeTime).ToArray();
                if (l != null && l.Length > 0)
                    return 2;
            }
            long[] b = (from p in gameHolder.CoinPiles
                        where p != null && p.Location == pt && p.LifeTime > 0
                        select p.LifeTime).ToArray();
            if (b != null && b.Length > 0)
                return 1;
            return 0;
        }

        /// <summary>
        /// Wether there is a map obstructacle
        /// </summary>
        /// <param name="pt">Location to be examined</param>
        /// <returns>bool</returns>
        private bool canMove(Point pt)
        {
            if (pt == null) return false;
            if (pt.X < 0 || pt.Y < 0 || pt.X >= GameSettings.GridSize || pt.Y >= GameSettings.GridSize)
            {
                return false;
            }
            /*Player[] b = new Player[] { };
             b = (from p in gameHolder.Players
                 where p != null && p.Location == pt
                 select p).ToArray(); */
            if (/* b.Length > 0 || */
                gameHolder.StoneWalls.Contains(pt) ||
                gameHolder.WaterBodies.Contains(pt) ||
                (gameHolder.BrickWalls.ContainsKey(pt) && (int)gameHolder.BrickWalls[pt] < 4))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Wether there is a player
        /// </summary>
        /// <param name="pt">Location to be examined</param>
        /// <returns>bool</returns>
        private bool HasPlayer(Point pt)
        {
            Player[] b = new Player[] { };
            b = (from p in gameHolder.Players
                 where p != null && p.Location == pt && p.Health > 0
                 select p).ToArray();
            return b.Length > 0;
        }

        #endregion

        #region CoinPile absent action

        /// <summary>
        /// Try offencive for enemy
        /// </summary>
        /// <returns></returns>
        private string GetOffenciveActionEnemy()
        {
            if (gameHolder.StateOfGame == ReplyTypes.GAME_START)
            {
                string offensiveAction = "";

                offensiveAction = ShootEnemy();
                if (offensiveAction != "Not Shootable")
                {
                    return offensiveAction;
                }

                //offensiveAction = ShootEnemyInFuture();
                //if (offensiveAction != "Not Shootable")
                //{
                //	return offensiveAction;
                //}

            }
            return "";
        }

        /// <summary>
        /// Try offencive action
        /// </summary>
        /// <returns></returns>
        private string GetOffensiveAction()
        {
            if (gameHolder.StateOfGame == ReplyTypes.GAME_START)
            {
                string offensiveAction = "";

                //offensiveAction = ShootEnemy();
                //if (offensiveAction != "Not Shootable")
                //{
                //	return offensiveAction;
                //}

                offensiveAction = ShootEnemyInFuture();
                if (offensiveAction != "Not Shootable")
                {
                    if (gameHolder.PlayerId >= 0 && gameHolder.Players[gameHolder.PlayerId] != null)
                    {
                        Player me = gameHolder.Players[gameHolder.PlayerId];
                        if (me.Health > minHealth)
                        {
                            return offensiveAction;
                        }
                    }
                }

                offensiveAction = ShootBrickWall();
                if (offensiveAction != "Not Shootable")
                {
                    return offensiveAction;
                }

                offensiveAction = ShootBrickWallInFuture();
                if (offensiveAction != "Not Shootable")
                {
                    return offensiveAction;
                }
            }
            return "";
        }

        /// <summary>
        /// Calculate possibility to shoot enemy
        /// </summary>
        /// <returns></returns>
        private string ShootEnemy()
        {
            /*
			1. check for enemy in line of sight
					-start from north and go clockwise
					-when found break
					-No obstacles in between

					2. if enemy in line of sight check if our tank is facing the tank
						-if in line of sight then issue shoot command
						-if not facing the enemy issue command to face the enemy
			*/

            //direction to check
            /*
				North = 0, -> x constant y reduce
				East = 1, -> x increase y constant
				South = 2, -> x constant y increase
				West = 3 -> x decrease y constant
			*/

            if (gameHolder.Players.Count() <= 1 || IsAllEnemiesDead()) return "Not Shootable";


            Player me = gameHolder.Players[gameHolder.PlayerId];
            Point currentPt = me.Location;
            int currentX = currentPt.X;
            int currentY = currentPt.Y;

            int enemyDirection = -1;
            for (int itr = 0; itr < 4; itr++)//Iterate 4 directions
            {
                if (itr == 0)//North = 0, -> x constant y reduce
                {
                    if (GetShootableEnemyDirection(MatchAxis.X, currentX, currentY) == 0)
                    {
                        enemyDirection = 0;
                        break;
                    }
                }
                else if (itr == 1)//East = 1, -> x increase y constant
                {
                    if (GetShootableEnemyDirection(MatchAxis.Y, currentY, currentX) == 1)
                    {
                        enemyDirection = 1;
                        break;
                    }
                }
                else if (itr == 2)//South = 2, -> x constant y increase
                {
                    if (GetShootableEnemyDirection(MatchAxis.X, currentX, currentY) == 2)
                    {
                        enemyDirection = 2;
                        break;
                    }
                }
                else if (itr == 3)//West = 3 -> x decrease y constant
                {
                    if (GetShootableEnemyDirection(MatchAxis.Y, currentY, currentX) == 3)
                    {
                        enemyDirection = 3;
                        break;
                    }
                }

            }

            //Choose to shoot or face enemy
            switch (enemyDirection)
            {
                case 0:
                    return me.Direction == Directions.North ? GameCommands.Shoot : GameCommands.MoveUp;
                case 1:
                    return me.Direction == Directions.East ? GameCommands.Shoot : GameCommands.MoveRight;
                case 2:
                    return me.Direction == Directions.South ? GameCommands.Shoot : GameCommands.MoveDown;
                case 3:
                    return me.Direction == Directions.West ? GameCommands.Shoot : GameCommands.MoveLeft;
            }

            return "Not Shootable";

        }

        /// <summary>
        /// Calculate possibility to shoot brick
        /// </summary>
        /// <returns></returns>
        private string ShootBrickWall()
        {
            /*
			3.Else check for brakable bricks in line of sight
					  - start from north and go clockwise
					  - when found break
					-Brick health must be grater than 0

					4.If facing brick issue shoor command
					5.Else turn tank to face the brick
			*/

            Player me = gameHolder.Players[gameHolder.PlayerId];
            Point currentPt = me.Location;
            int currentX = currentPt.X;
            int currentY = currentPt.Y;

            int brickDirection = -1;
            for (int itr = 0; itr < 4; itr++)//Iterate 4 directions
            {
                if (itr == 0)//North = 0, -> x constant y reduce
                {
                    if (GetShootableBrickWallDirection(MatchAxis.X, currentX, currentY) == 0)
                    {
                        brickDirection = 0;
                        break;
                    }
                }
                if (itr == 1)//East = 1, -> x increase y constant
                {
                    if (GetShootableBrickWallDirection(MatchAxis.Y, currentY, currentX) == 1)
                    {
                        brickDirection = 1;
                        break;
                    }
                }
                if (itr == 2)//South = 2, -> x constant y increase
                {
                    if (GetShootableBrickWallDirection(MatchAxis.X, currentX, currentY) == 2)
                    {
                        brickDirection = 2;
                        break;
                    }
                }
                if (itr == 3)//West = 3 -> x decrease y constant
                {
                    if (GetShootableBrickWallDirection(MatchAxis.Y, currentY, currentX) == 3)
                    {
                        brickDirection = 3;
                        break;
                    }
                }

            }

            //Choose to shoot or face brick
            switch (brickDirection)
            {
                case 0:
                    return me.Direction == Directions.North ? GameCommands.Shoot : GameCommands.MoveUp;
                case 1:
                    return me.Direction == Directions.East ? GameCommands.Shoot : GameCommands.MoveRight;
                case 2:
                    return me.Direction == Directions.South ? GameCommands.Shoot : GameCommands.MoveDown;
                case 3:
                    return me.Direction == Directions.West ? GameCommands.Shoot : GameCommands.MoveLeft;
            }

            return "Not Shootable";
        }

        /// <summary>
        /// Try to make a move to shoot a enemy in the future
        /// </summary>
        /// <returns></returns>
        private string ShootEnemyInFuture()
        {
            if (gameHolder.Players.Count() <= 1 || IsAllEnemiesDead()) return "Not Shootable";

            Player me = gameHolder.Players[gameHolder.PlayerId];
            Point currentPt = me.Location;
            int currentDirection = (int)me.Direction;
            int currentX = currentPt.X;
            int currentY = currentPt.Y;

            //Try to move in current direction
            if (currentDirection == 0 && canMove(new Point(currentX, currentY - 1)))//North = 0, -> x constant y reduce
            {

                int shootableDir = GetShootableEnemyDirection(MatchAxis.Y, currentY - 1, currentX);
                if (shootableDir == 1 || shootableDir == 3)
                {
                    return GameCommands.MoveUp;
                }

            }
            else if (currentDirection == 1 && canMove(new Point(currentX + 1, currentY)))//East = 1, -> x increase y constant
            {
                int shootableDir = GetShootableEnemyDirection(MatchAxis.X, currentX + 1, currentY);
                if (shootableDir == 0 || shootableDir == 2)
                {
                    return GameCommands.MoveRight;
                }
            }
            else if (currentDirection == 2 && canMove(new Point(currentX, currentY + 1)))//South = 2, -> x constant y increase
            {
                int shootableDir = GetShootableEnemyDirection(MatchAxis.Y, currentY + 1, currentX);
                if (shootableDir == 1 || shootableDir == 3)
                {
                    return GameCommands.MoveDown;
                }
            }
            else if (currentDirection == 3 && canMove(new Point(currentX - 1, currentY)))//West = 3 -> x decrease y constant
            {
                int shootableDir = GetShootableEnemyDirection(MatchAxis.X, currentX - 1, currentY);
                if (shootableDir == 0 || shootableDir == 2)
                {
                    return GameCommands.MoveLeft;
                }
            }


            for (int itr = 0; itr < 4; itr++)//Iterate 4 directions
            {
                if (itr == currentDirection)//Already checked it above
                {
                    continue;
                }

                if (itr == 0 && canMove(new Point(currentX, currentY - 1)))//North = 0, -> x constant y reduce
                {
                    int shootableDir = GetShootableEnemyDirection(MatchAxis.Y, currentY - 1, currentX);
                    if (shootableDir == 1 || shootableDir == 3)
                    {
                        return GameCommands.MoveUp;
                    }
                }
                else if (itr == 1 && canMove(new Point(currentX + 1, currentY)))//East = 1, -> x increase y constant
                {
                    int shootableDir = GetShootableEnemyDirection(MatchAxis.X, currentX + 1, currentY);
                    if (shootableDir == 0 || shootableDir == 2)
                    {
                        return GameCommands.MoveRight;
                    }
                }
                else if (itr == 2 && canMove(new Point(currentX, currentY + 1)))//South = 2, -> x constant y increase
                {
                    int shootableDir = GetShootableEnemyDirection(MatchAxis.Y, currentY + 1, currentX);
                    if (shootableDir == 1 || shootableDir == 3)
                    {
                        return GameCommands.MoveDown;
                    }
                }
                else if (itr == 3 && canMove(new Point(currentX - 1, currentY)))//West = 3 -> x decrease y constant
                {
                    int shootableDir = GetShootableEnemyDirection(MatchAxis.X, currentX - 1, currentY);
                    if (shootableDir == 0 || shootableDir == 2)
                    {
                        return GameCommands.MoveLeft;
                    }
                }

            }

            return "Not Shootable";
        }

        /// <summary>
        ///  Try to make a move to shoot a brick wall in the future
        /// </summary>
        /// <returns></returns>
        private string ShootBrickWallInFuture()
        {
            Player me = gameHolder.Players[gameHolder.PlayerId];
            Point currentPt = me.Location;
            int currentDirection = (int)me.Direction;
            int currentX = currentPt.X;
            int currentY = currentPt.Y;

            //Try to move in current direction
            if (currentDirection == 0 && canMove(new Point(currentX, currentY - 1)))//North = 0, -> x constant y reduce
            {

                int shootableDir = GetShootableBrickWallDirection(MatchAxis.Y, currentY - 1, currentX);
                if (shootableDir == 1 || shootableDir == 3)
                {
                    return GameCommands.MoveUp;
                }

            }
            else if (currentDirection == 1 && canMove(new Point(currentX + 1, currentY)))//East = 1, -> x increase y constant
            {
                int shootableDir = GetShootableBrickWallDirection(MatchAxis.X, currentX + 1, currentY);
                if (shootableDir == 0 || shootableDir == 2)
                {
                    return GameCommands.MoveRight;
                }
            }
            else if (currentDirection == 2 && canMove(new Point(currentX, currentY + 1)))//South = 2, -> x constant y increase
            {
                int shootableDir = GetShootableBrickWallDirection(MatchAxis.Y, currentY + 1, currentX);
                if (shootableDir == 1 || shootableDir == 3)
                {
                    return GameCommands.MoveDown;
                }
            }
            else if (currentDirection == 3 && canMove(new Point(currentX - 1, currentY)))//West = 3 -> x decrease y constant
            {
                int shootableDir = GetShootableBrickWallDirection(MatchAxis.X, currentX - 1, currentY);
                if (shootableDir == 0 || shootableDir == 2)
                {
                    return GameCommands.MoveLeft;
                }
            }


            for (int itr = 0; itr < 4; itr++)//Iterate 4 directions
            {
                if (itr == currentDirection)//Already checked it above
                {
                    continue;
                }

                if (itr == 0 && canMove(new Point(currentX, currentY - 1)))//North = 0, -> x constant y reduce
                {
                    int shootableDir = GetShootableBrickWallDirection(MatchAxis.Y, currentY - 1, currentX);
                    if (shootableDir == 1 || shootableDir == 3)
                    {
                        return GameCommands.MoveUp;
                    }
                }
                else if (itr == 1 && canMove(new Point(currentX + 1, currentY)))//East = 1, -> x increase y constant
                {
                    int shootableDir = GetShootableBrickWallDirection(MatchAxis.X, currentX + 1, currentY);
                    if (shootableDir == 0 || shootableDir == 2)
                    {
                        return GameCommands.MoveRight;
                    }
                }
                else if (itr == 2 && canMove(new Point(currentX, currentY + 1)))//South = 2, -> x constant y increase
                {
                    int shootableDir = GetShootableBrickWallDirection(MatchAxis.Y, currentY + 1, currentX);
                    if (shootableDir == 1 || shootableDir == 3)
                    {
                        return GameCommands.MoveDown;
                    }
                }
                else if (itr == 3 && canMove(new Point(currentX - 1, currentY)))//West = 3 -> x decrease y constant
                {
                    int shootableDir = GetShootableBrickWallDirection(MatchAxis.X, currentX - 1, currentY);
                    if (shootableDir == 0 || shootableDir == 2)
                    {
                        return GameCommands.MoveLeft;
                    }
                }

            }

            return "Not Shootable";
        }

        /// <summary>
        /// Check if there are no shootable enemies
        /// </summary>
        /// <returns></returns>
        private bool IsAllEnemiesDead()
        {
            Player[] players = new Player[5];
            Array.Copy(gameHolder.Players, players, gameHolder.Players.Count());

            for (int plrItr = 0; plrItr < players.Count(); plrItr++) //Iterte players
            {
                if (gameHolder.PlayerId != plrItr && players[plrItr] != null && players[plrItr].Health > 0)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Check if possible enemy is in line of sight
        /// </summary>
        /// <param name="axis">Axis along witch to check for enemy</param>
        /// <param name="matchCoordinate">Axis coordinate of our tank to match</param>
        /// <param name="diffCoordinate">The coordinate of our tank which can differ form the compared tank</param>
        /// <returns></returns>
        private int GetShootableEnemyDirection(MatchAxis axis, int matchCoordinate, int diffCoordinate)
        {
            Player[] players = new Player[5];
            Array.Copy(gameHolder.Players, players, gameHolder.Players.Count());

            for (int plrItr = 0; plrItr < players.Count(); plrItr++)//Iterte players
            {
                if (gameHolder.PlayerId != plrItr && players[plrItr] != null && players[plrItr].Health > 0)
                {
                    switch (axis)
                    {
                        case MatchAxis.X:
                            if (players[plrItr].Location.X == matchCoordinate && CheckForNoObstacles(axis, matchCoordinate, diffCoordinate, players[plrItr].Location.Y, true))
                            {
                                if (players[plrItr].Location.Y < diffCoordinate)
                                {
                                    return 0;
                                }
                                if (players[plrItr].Location.Y > diffCoordinate)
                                {
                                    return 2;
                                }
                            }
                            break;
                        case MatchAxis.Y:
                            if (players[plrItr].Location.Y == matchCoordinate && CheckForNoObstacles(axis, matchCoordinate, diffCoordinate, players[plrItr].Location.X, true))
                            {
                                if (players[plrItr].Location.X < diffCoordinate)
                                {
                                    return 3;
                                }
                                if (players[plrItr].Location.X > diffCoordinate)
                                {
                                    return 1;
                                }
                            }
                            break;
                    }

                }
            }
            return -1;
        }

        /// <summary>
        /// Check if possible brick wall is in line of sight
        /// </summary>
        /// <param name="axis">Axis along witch to check for brick wall</param>
        /// <param name="matchCoordinate">Axis coordinate of our tank to match</param>
        /// <param name="diffCoordinate">The coordinate of our tank which can differ form the compared brick</param>
        /// <returns></returns>
        private int GetShootableBrickWallDirection(MatchAxis axis, int matchCoordinate, int diffCoordinate)
        {
            Hashtable brickWalls = Hashtable.Synchronized(gameHolder.BrickWalls);

            foreach (DictionaryEntry brickWall in brickWalls)
            {
                if ((int)brickWall.Value == 4) continue;

                Point brickWallLocation = (Point)brickWall.Key;

                switch (axis)
                {
                    case MatchAxis.X:
                        if (brickWallLocation.X == matchCoordinate && CheckForNoObstacles(axis, matchCoordinate, diffCoordinate, brickWallLocation.Y, false))
                        {
                            if (brickWallLocation.Y < diffCoordinate)
                            {
                                return 0;
                            }
                            else if (brickWallLocation.Y > diffCoordinate)
                            {
                                return 2;
                            }
                        }
                        break;
                    case MatchAxis.Y:
                        if (brickWallLocation.Y == matchCoordinate && CheckForNoObstacles(axis, matchCoordinate, diffCoordinate, brickWallLocation.X, false))
                        {
                            if (brickWallLocation.X < diffCoordinate)
                            {
                                return 3;
                            }
                            else if (brickWallLocation.X > diffCoordinate)
                            {
                                return 1;
                            }
                        }
                        break;
                }
            }

            return -1;
        }

        /// <summary>
        /// Check for obstacles in the line of sight between our tank and enemy or brick wall
        /// </summary>
        /// <param name="axis">Axis along witch to check for obstacles</param>
        /// <param name="matchCoordinate">Axis coordinate of our tank to match</param>
        /// <param name="startCoordinate">The varing coordinate of our tank</param>
        /// <param name="endCoordinate">The varing coordinate of enemy or brick wall</param>
        /// <param name="checkForBrickWalls">used to choose between enemy or brick wall</param>
        /// <returns>return true if no obstacles in line of sight</returns>
        private bool CheckForNoObstacles(MatchAxis axis, int matchCoordinate, int startCoordinate, int endCoordinate, bool checkForBrickWalls)
        {

            if (checkForBrickWalls)
            {
                Hashtable brickWalls = Hashtable.Synchronized(gameHolder.BrickWalls);

                //Check obstructing brick walls
                foreach (DictionaryEntry brickWall in brickWalls)
                {
                    if ((int)brickWall.Value == 4) continue;

                    Point brickWallLocation = (Point)brickWall.Key;

                    switch (axis)
                    {
                        case MatchAxis.X:
                            if (brickWallLocation.X == matchCoordinate)
                            {
                                if (startCoordinate < endCoordinate)//Check in south direction
                                {
                                    if (startCoordinate < brickWallLocation.Y && brickWallLocation.Y < endCoordinate)
                                    {
                                        return false;
                                    }
                                }
                                else if (startCoordinate > endCoordinate)//Check in North direction
                                {
                                    if (endCoordinate < brickWallLocation.Y && brickWallLocation.Y < startCoordinate)
                                    {
                                        return false;
                                    }
                                }
                            }
                            break;
                        case MatchAxis.Y:
                            if (brickWallLocation.Y == matchCoordinate)
                            {
                                if (startCoordinate < endCoordinate)//Check in East direction
                                {
                                    if (startCoordinate < brickWallLocation.X && brickWallLocation.X < endCoordinate)
                                    {
                                        return false;
                                    }
                                }
                                else if (startCoordinate > endCoordinate)//Check in West direction
                                {
                                    if (endCoordinate < brickWallLocation.X && brickWallLocation.X < startCoordinate)
                                    {
                                        return false;
                                    }
                                }
                            }
                            break;
                    }
                }
            }

            List<Point> stoneWalls = gameHolder.StoneWalls.ToList();

            //Check obstructing Stone walls
            for (var i = 0; i < stoneWalls.Count; i++)
            {
                switch (axis)
                {
                    case MatchAxis.X:
                        if (stoneWalls[i].X == matchCoordinate)
                        {
                            if (startCoordinate < endCoordinate)//Check in south direction
                            {
                                if (startCoordinate < stoneWalls[i].Y && stoneWalls[i].Y < endCoordinate)
                                {
                                    return false;
                                }
                            }
                            else if (startCoordinate > endCoordinate)//Check in North direction
                            {
                                if (endCoordinate < stoneWalls[i].Y && stoneWalls[i].Y < startCoordinate)
                                {
                                    return false;
                                }
                            }
                        }
                        break;
                    case MatchAxis.Y:
                        if (stoneWalls[i].Y == matchCoordinate)
                        {
                            if (startCoordinate < endCoordinate)//Check in East direction
                            {
                                if (startCoordinate < stoneWalls[i].X && stoneWalls[i].X < endCoordinate)
                                {
                                    return false;
                                }
                            }
                            else if (startCoordinate > endCoordinate)//Check in West direction
                            {
                                if (endCoordinate < stoneWalls[i].X && stoneWalls[i].X < startCoordinate)
                                {
                                    return false;
                                }
                            }
                        }
                        break;
                }
            }

            return true;
        }

        #endregion

    }

}
