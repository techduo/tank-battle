﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace TanksGame
{
	class GameSettings
	{
		//Resolution
		public static int ReslutionWidth = 800;
		public static int ReslutionHeight = 450;

		//Game
		public static string Title = "Tank Game Client";
		public static string ServerIpAddress = "127.0.0.1";
		public static int GridSize = 10;
		public static int TileSize = ReslutionHeight / GridSize;

        /// <summary>
        /// Refreshes Tile size
        /// </summary>
        internal static void Refresh()
        {
            TileSize = ReslutionHeight / GridSize;
        }

        /// <summary>
        /// Set window mode to FullScren
        /// </summary>
        /// <param name="graphics"></param>
        internal static void SetFullScreen(GraphicsDeviceManager graphics) {
            graphics.IsFullScreen = true;
            GameSettings.ReslutionWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            GameSettings.ReslutionHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            GameSettings.Refresh();
        }

        /// <summary>
        /// Changes screen size
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        internal static void SetScreenSize(GraphicsDeviceManager graphics, int width, int height)
        {
            graphics.IsFullScreen = false;
            GameSettings.ReslutionWidth = width;
            GameSettings.ReslutionHeight = height;
            GameSettings.Refresh();
        }
    }
}
