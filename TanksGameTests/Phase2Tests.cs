﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TanksGame.Objects.ReplyObjects;
using TanksGame.Syntax;
using TanksGame.Objects;

namespace TankGameTest
{
    [TestClass]
    public class Phase2Tests
    {
        #region Parser Tests

        /// <summary>
        /// Test null input
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NULL()
        {
            String input = null;
            Reply actual = Parser.GetInstance.Parse(input);
        }

        /// <summary>
        /// Test empty input
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void EMPTY()
        {
            string input = "  ";
            Reply actual = Parser.GetInstance.Parse(input);
        }

        //Test no data inputs

        /// <summary>
        /// Test PLAYERS_FULL
        /// </summary>
        [TestMethod]
        public void PLAYERS_FULL()
        {
            String input = "PLAYERS_FULL#";
            ReplyTypes expected = ReplyTypes.PLAYERS_FULL;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test ALREADY_ADDED
        /// </summary>
        [TestMethod]
        public void ALREADY_ADDED()
        {
            String input = "ALREADY_ADDED#";
            ReplyTypes expected = ReplyTypes.ALREADY_ADDED;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test GAME_ALREADY_STARTED
        /// </summary>
        [TestMethod]
        public void GAME_ALREADY_STARTED()
        {
            String input = "GAME_ALREADY_STARTED#";
            ReplyTypes expected = ReplyTypes.GAME_ALREADY_STARTED;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test OBSTACLE
        /// </summary>
        [TestMethod]
        public void OBSTACLE()
        {
            String input = "OBSTACLE;12#";
            ReplyTypes expected = ReplyTypes.OBSTACLE;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test CELL_OCCUPIED
        /// </summary>
        [TestMethod]
        public void CELL_OCCUPIED()
        {
            String input = "CELL_OCCUPIED#";
            ReplyTypes expected = ReplyTypes.CELL_OCCUPIED;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test DEAD
        /// </summary>
        [TestMethod]
        public void DEAD()
        {
            String input = "DEAD#";
            ReplyTypes expected = ReplyTypes.DEAD;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test TOO_QUICK
        /// </summary>
        [TestMethod]
        public void TOO_QUICK()
        {
            String input = "TOO_QUICK#";
            ReplyTypes expected = ReplyTypes.TOO_QUICK;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test INVALID_CELL
        /// </summary>
        [TestMethod]
        public void INVALID_CELL()
        {
            String input = "INVALID_CELL#";
            ReplyTypes expected = ReplyTypes.INVALID_CELL;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test GAME_HAS_FINISHED
        /// </summary>
        [TestMethod]
        public void GAME_HAS_FINISHED()
        {
            String input = "GAME_HAS_FINISHED#";
            ReplyTypes expected = ReplyTypes.GAME_HAS_FINISHED;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test GAME_NOT_STARTED_YET
        /// </summary>
        [TestMethod]
        public void GAME_NOT_STARTED_YET()
        {
            String input = "GAME_NOT_STARTED_YET#";
            ReplyTypes expected = ReplyTypes.GAME_NOT_STARTED_YET;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test NOT_A_VALID_CONTESTANT
        /// </summary>
        [TestMethod]
        public void NOT_A_VALID_CONTESTANT()
        {
            String input = "NOT_A_VALID_CONTESTANT#";
            ReplyTypes expected = ReplyTypes.NOT_A_VALID_CONTESTANT;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test GAME_FINISHED
        /// </summary>
        [TestMethod]
        public void GAME_FINISHED()
        {
            String input = "GAME_FINISHED#";
            ReplyTypes expected = ReplyTypes.GAME_FINISHED;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        //Test with data

        /// <summary>
        /// Test JOIN_ACCEPT_TYPE
        /// </summary>
        [TestMethod]
        public void JOIN_ACCEPT_TYPE()
        {
            String input = "S:P0;0,0;0:P1;0,9;0:P2;9,0;0#";
            ReplyTypes expected = ReplyTypes.GAME_START;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test MAP_DATA_TYPE
        /// </summary>
        [TestMethod]
        public void MAP_DATA_TYPE()
        {
            String input = "I:P0:7,2;8,6;4,2;5,7;1,3;7,8;2,4:6,8;5,3;7,1;5,8;8,1;0,3;3,1;1,4:3,6;4,7;2,1;4,8;6,3;7,4;2,6;9,8;4,3;8,4#";
            ReplyTypes expected = ReplyTypes.MAP_DATA;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test GLOBAL_DATA_UPDATE_TYPE
        /// </summary>
        [TestMethod]
        public void GLOBAL_DATA_UPDATE_TYPE()
        {
            String input = "G:P0;1,2;2;1;110;3054;3174:P1;5,5;1;0;100;6811;6811:P2;7,5;1;0;110;2185;2185:7,2,4;8,6,0;4,2,4;5,7,0;1,3,4;7,8,0;2,4,0#";
            ReplyTypes expected = ReplyTypes.GLOBAL_DATA_UPDATE;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test COIN_DATA_TYPE
        /// </summary>
        [TestMethod]
        public void COIN_DATA_TYPE()
        {
            String input = "C:1,7:27315:1074#";
            ReplyTypes expected = ReplyTypes.COIN_DATA;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

        /// <summary>
        /// Test LIFE_PACK_DATA_TYPE
        /// </summary>
        [TestMethod]
        public void LIFE_PACK_DATA_TYPE()
        {
            String input = "L:8,9:71559#";
            ReplyTypes expected = ReplyTypes.LIFE_PACK_DATA;

            Reply actual = Parser.GetInstance.Parse(input);

            Assert.AreEqual(expected, actual.MessageType);

        }

    }
    #endregion
}
